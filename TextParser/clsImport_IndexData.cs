﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ontology_Module;
using OntologyClasses.BaseClasses;
using ElasticSearchLogging_Module;
using OntologyAppDBConnector;

namespace TextParser
{
    public class clsImport_IndexData
    {
        private clsLocalConfig objLocalConfig;

        private clsDataWork_Import objDataWork_Import;


        private clsLogging objLogging;

        private clsAppDBLevel objAppDBLevel_Index;

        private OntologyModDBConnector objDBLevel1;

        private clsTransaction objTransaction;
        private clsRelationConfig objRelationConfig;

        private clsOntologyItem objOItem_TextParser;


        public clsImport_IndexData(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;

            Initialize();
        }

        public clsOntologyItem ImportIndexData(clsOntologyItem OItem_TextParser, List<clsField> fieldList, string query = null, List<string> docIds = null)
        {
            var importDateTime = DateTime.Now;
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var importStampField = fieldList.FirstOrDefault(fieldItem => fieldItem.ID_MetaField == objLocalConfig.OItem_object_importstamp.GUID);

            objOItem_TextParser = OItem_TextParser;

            objOItem_Result = objDataWork_Import.GetData(objOItem_TextParser, fieldList);

            List<ImportItem> entityMetaItems = null;
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                entityMetaItems = objDataWork_Import.FieldItems.Where(fieldItem => fieldItem.IdentificationItem && fieldItem.OrderId == 1 && fieldItem.Type_RefItem == objLocalConfig.Globals.Type_Class).ToList();

                if (entityMetaItems.Count != 1)
                {
                    return objLocalConfig.Globals.LState_Error.Clone();
                }
            }

            var entityMetaFilterItems =  objDataWork_Import.FieldItems.Where(fieldItem => fieldItem.IdentificationItem && fieldItem.OrderId != 1).ToList();
            var metaItems = objDataWork_Import.FieldItems.Where(fieldItem => (fieldItem.IdentificationItem && fieldItem.OrderId == 1) == false).ToList();

            int port;
            if (!int.TryParse(objDataWork_Import.OItem_Port.Name, out port))
            {
                return objLocalConfig.Globals.LState_Error.Clone();
            }

            objAppDBLevel_Index = new clsAppDBLevel(objDataWork_Import.OItem_Server.Name, port, objDataWork_Import.OItem_Index.Name.ToLower(), objLocalConfig.Globals.SearchRange, objLocalConfig.Globals.Session);
            var objDocuments = objAppDBLevel_Index.GetData_Documents(objDataWork_Import.OItem_Index.Name.ToLower(), objDataWork_Import.OItem_Type.Name.ToLower(), strQuery: query ?? "*");

            if (docIds != null && docIds.Any())
            {
                objDocuments = (from docItem in  objDocuments
                                join docId in docIds on docItem.Id equals docId
                                select docItem).ToList();
            }
            var todo = objDocuments.Count;
            var done = 0;
            var doneInsert = false;

            var entityMetaItem = entityMetaItems.First();
            foreach (var objDoc in objDocuments)
            {
                objTransaction.ClearItems();
                objLocalConfig.objLogging.Init_Document(objDoc.Id);
                objLocalConfig.objLogging.Add_DictEntry("timestamp_log", importDateTime);
                objLocalConfig.objLogging.Add_DictEntry("timestamp", DateTime.Now);

                objLocalConfig.objLogging.Add_DictEntry("index", objDataWork_Import.OItem_Index.Name + "@" + objDataWork_Import.OItem_Server.Name);
                objLocalConfig.objLogging.Add_DictEntry("docid", objDoc.Id);

                
                if (!objDoc.Dict.ContainsKey (entityMetaItem.FieldItem.Name_Field))
                {
                    objLocalConfig.objLogging.Add_DictEntry("step", "Get Entity-Identificator");
                    objLocalConfig.objLogging.Add_DictEntry("error", true);
                    objLocalConfig.objLogging.Finish_Document();
                    continue;
                }

                var identificatorObject = objDoc.Dict[entityMetaItem.FieldItem.Name_Field];
                
                if (identificatorObject == null)
                {
                    objLocalConfig.objLogging.Add_DictEntry("step", "Get Entity-Identificator");
                    objLocalConfig.objLogging.Add_DictEntry("error", true);
                    objLocalConfig.objLogging.Finish_Document();
                    continue;

                }

                var nameIdentificator = identificatorObject.ToString();
                if (entityMetaItems.First().UseDocId)
                {
                    nameIdentificator = objDoc.Id;
                }
                
                var entityItems = entityMetaItems.First().GetEntityItems(nameIdentificator);

                if (entityItems == null)
                {
                    objLocalConfig.objLogging.Add_DictEntry("step", "Get ontology Entities");
                    objLocalConfig.objLogging.Add_DictEntry("error", true);
                    objLocalConfig.objLogging.Finish_Document();
                    continue;
                }

                clsOntologyItem entityItem = null;
                if (entityItems.Any())
                {
                    foreach(var filterItem in entityMetaFilterItems)
                    {
                        object valueObject = null;
                        if (objDoc.Dict.ContainsKey(filterItem.FieldItem.Name_Field))
                        {
                            valueObject = objDoc.Dict[filterItem.FieldItem.Name_Field];
                            if (filterItem.Type_RefItem == objLocalConfig.Globals.Type_AttributeType)
                            {
                                entityItems = filterItem.GetFilteredAttributeItems(entityItems, valueObject);

                            }
                            else if (valueObject != null)
                            {
                                entityItems = filterItem.GetFilteredRelated(entityItems, valueObject.ToString());
                            }

                        }
                        
                    }

                    if (entityItems.Count == 1)
                    {
                        entityItem = entityItems.First();
                    }
                    else if (!entityItems.Any())
                    {
                        
                        entityItem = new clsOntologyItem
                        {
                            GUID = entityMetaItem.UseDocId ? objDoc.Id : objLocalConfig.Globals.NewGUID,
                            Name = identificatorObject.ToString(),
                            GUID_Parent = entityMetaItem.Id_RefItem,
                            Type = objLocalConfig.Globals.Type_Object
                        };

                        var result = objTransaction.do_Transaction(entityItem);
                        if (result.GUID == objLocalConfig.Globals.LState_Error.GUID)
                        {
                            objLocalConfig.objLogging.Add_DictEntry("step", "Save entity-item");
                            objLocalConfig.objLogging.Add_DictEntry("error", true);
                            objLocalConfig.objLogging.Finish_Document();
                            continue;
                        }
                    }
                    else
                    {
                        objLocalConfig.objLogging.Add_DictEntry("step", "Get entity-item");
                        objLocalConfig.objLogging.Add_DictEntry("error", true);
                        objLocalConfig.objLogging.Add_DictEntry("desc", "To much entity-items");
                        objLocalConfig.objLogging.Finish_Document();
                        continue;
                    }
                    
                }
                else
                {
                    entityItem = new clsOntologyItem
                    {
                        GUID = entityMetaItem.UseDocId ? objDoc.Id : objLocalConfig.Globals.NewGUID,
                        Name = identificatorObject.ToString(),
                        GUID_Parent = entityMetaItem.Id_RefItem,
                        Type = objLocalConfig.Globals.Type_Object
                    };

                    var result = objTransaction.do_Transaction(entityItem);
                    if (result.GUID == objLocalConfig.Globals.LState_Error.GUID)
                    {
                        objLocalConfig.objLogging.Add_DictEntry("step", "Save entity-item");
                        objLocalConfig.objLogging.Add_DictEntry("error", true);
                        objLocalConfig.objLogging.Finish_Document();
                        continue;
                    }
                }

                var error = false;
                var countFieldsToDo = metaItems.Count;
                var countFieldsDone = 0;
                foreach(var metaItem in metaItems)
                {
                    if (objDoc.Dict.ContainsKey(metaItem.FieldItem.Name_Field))
                    {
                        var valueObject = objDoc.Dict[metaItem.FieldItem.Name_Field];
                        if (valueObject != null)
                        {
                            if (metaItem.Type_RefItem == objLocalConfig.Globals.Type_AttributeType)
                            {
                                var result = metaItem.SaveAttributes(entityItem, valueObject, objTransaction);
                                if (result.GUID == objLocalConfig.Globals.LState_Error.GUID)
                                {
                                    error = true;
                                    objTransaction.rollback();

                                    break;
                                }
                            }
                            else if (metaItem.Type_RefItem == objLocalConfig.Globals.Type_Class)
                            {
                                var result = metaItem.SaveRelated(entityItem, valueObject.ToString(), objTransaction);
                                if (result.GUID == objLocalConfig.Globals.LState_Error.GUID)
                                {
                                    error = true;
                                    objTransaction.rollback();

                                    break;
                                }
                            }
                            else
                            {
                                error = true;
                                objTransaction.rollback();

                                break;
                            }
                        }
                    
                        
                    }
                    countFieldsDone++;
                }

                if (!error && importStampField != null)
                {
                    if (objDoc.Dict.ContainsKey(importStampField.Name_Field))
                    {
                        objDoc.Dict[importStampField.Name_Field] = DateTime.Now;
                    }
                    else
                    {
                        objDoc.Dict.Add(importStampField.Name_Field, DateTime.Now);
                    }

                    var result = objAppDBLevel_Index.Save_Documents(new List<clsAppDocuments> { objDoc }, objDataWork_Import.OItem_Type.Name);

                    objLocalConfig.objLogging.Add_DictEntry("SetImportStamp", result.GUID == objLocalConfig.Globals.LState_Success.GUID);

                }

                objLocalConfig.objLogging.Add_DictEntry("step", "Save Related");
                objLocalConfig.objLogging.Add_DictEntry("FieldsToSave", countFieldsToDo);
                objLocalConfig.objLogging.Add_DictEntry("FieldsSaved", countFieldsDone);
                objLocalConfig.objLogging.Add_DictEntry("error", error);
                objLocalConfig.objLogging.Finish_Document();

            }
            //int port;
            //if (int.TryParse(objDataWork_Import.OItem_Port.Name, out port))
            //{
            //    objAppDBLevel_Index = new clsAppDBLevel(objDataWork_Import.OItem_Server.Name, port, objDataWork_Import.OItem_Index.Name.ToLower(), objLocalConfig.Globals.SearchRange, objLocalConfig.Globals.Session);


            //    var objDocuments = objAppDBLevel_Index.GetData_Documents(objDataWork_Import.OItem_Index.Name.ToLower(), objDataWork_Import.OItem_Type.Name.ToLower(),strQuery:query ?? "*");

            //    //var objORel_FieldsToItems = objDataWork_Import.FieldItems.OrderBy(fi => fi.OrderId).ToList();

            //    //var todo = objDocuments.Count;
            //    //var done = 0;
            //    //var doneInsert = false;

            //    //var objORel_IdentificationFields = objORel_FieldsToItems.Where(idField => idField.OrderID == 1).ToList();
            //    //objORel_FieldsToItems = objORel_FieldsToItems.Where(idField => idField.OrderID != 1).ToList();

            //    foreach (var objDoc in objDocuments)
            //    {

            //        objLocalConfig.objLogging.Init_Document(objDoc.Id);
            //        objLocalConfig.objLogging.Add_DictEntry("timestamp_log", importDateTime);
            //        objLocalConfig.objLogging.Add_DictEntry("timestamp", DateTime.Now);

            //        objLocalConfig.objLogging.Add_DictEntry("index", objDataWork_Import.OItem_Index.Name + "@" + objDataWork_Import.OItem_Server.Name);
            //        objLocalConfig.objLogging.Add_DictEntry("docid", objDoc.Id);

            //        var objOList_Fields = new List<clsOntologyItem>();

            //        //if (doneInsert)
            //        //{
            //        //    done++;
            //        //}
            //        //if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID)
            //        //{
            //        //    objLocalConfig.objLogging.Add_DictEntry("error", "document");
            //        //    break;
            //        //}

            //        //doneInsert = false;
            //        //objTransaction.ClearItems();
            //        //var fieldCount = -1;
            //        //foreach (var objFieldToItem in objORel_FieldsToItems)
            //        //{
            //        //    fieldCount++;
            //        //    objLocalConfig.objLogging.Add_DictEntry("fieldtoitem_id_" + fieldCount, objFieldToItem.ID_Other);
            //        //    objLocalConfig.objLogging.Add_DictEntry("fieldtoitem_name_" + fieldCount, objFieldToItem.Name_Other);

            //        //    var objOItem_FieldToItem = new clsOntologyItem
            //        //    {
            //        //        GUID = objFieldToItem.ID_Other,
            //        //        Name = objFieldToItem.Name_Other,
            //        //        GUID_Parent = objFieldToItem.ID_Parent_Other,
            //        //        Type = objLocalConfig.Globals.Type_Object
            //        //    };

            //        //    var objFields = objDataWork_Import.Get_FieldsOfFieldToItem(objOItem_FieldToItem).ToList();

            //        //    var objOItem_Of_FieldToItem = objDataWork_Import.Get_OItemOfFieldToItem(objOItem_FieldToItem);

            //        //    var strName = "";

            //        //    foreach (var objOItem_Field in objFields)
            //        //    {
            //        //        if (objDoc.Dict.ContainsKey(objOItem_Field.Name))
            //        //        {
            //        //            if (objDoc.Dict[objOItem_Field.Name].ToString() != null)
            //        //            {
            //        //                strName += objDoc.Dict[objOItem_Field.Name].ToString();
            //        //            }
            //        //            else
            //        //            {
            //        //                strName += "unknown";
            //        //            }

            //        //        }
            //        //        else
            //        //        {
            //        //            strName += "unknown";
            //        //        }

            //        //    }


            //        //    if (objOItem_Of_FieldToItem.Type == objLocalConfig.Globals.Type_Class)
            //        //    {
            //        //        var objOSearch_Items = new List<clsOntologyItem> { new clsOntologyItem
            //        //        {
            //        //            Name = strName,
            //        //            GUID_Parent = objOItem_Of_FieldToItem.GUID,
            //        //            Type = objLocalConfig.Globals.Type_Object
            //        //        } };

            //        //        objOItem_Result = objDBLevel1.GetDataObjects(objOSearch_Items);

            //        //        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            //        //        {
            //        //            var foundItem = objDBLevel1.Objects1.FirstOrDefault(o => o.Name == strName);
            //        //            if (foundItem == null)
            //        //            {
            //        //                doneInsert = true;
            //        //                var objOItem_Object = new clsOntologyItem
            //        //                {
            //        //                    GUID = objLocalConfig.Globals.NewGUID,
            //        //                    Name = strName,
            //        //                    GUID_Parent = objOItem_Of_FieldToItem.GUID,
            //        //                    Type = objLocalConfig.Globals.Type_Object
            //        //                };

            //        //                objLocalConfig.objLogging.Add_DictEntry("object_id_" + fieldCount, objOItem_Object.GUID);
            //        //                objLocalConfig.objLogging.Add_DictEntry("object_name_" + fieldCount, objOItem_Object.Name);
            //        //                objLocalConfig.objLogging.Add_DictEntry("object_id_parent_" + fieldCount, objOItem_Object.GUID_Parent);
            //        //                objLocalConfig.objLogging.Add_DictEntry("object_operation_" + fieldCount, "insert");

            //        //                objOList_Fields.Add(objOItem_Object);

            //        //                objOItem_Result = objTransaction.do_Transaction(objOItem_Object);
            //        //            }
            //        //            else
            //        //            {
            //        //                objOList_Fields.Add(new clsOntologyItem
            //        //                {
            //        //                    GUID = foundItem.GUID,
            //        //                    Name = foundItem.Name,
            //        //                    GUID_Parent = foundItem.GUID_Parent,
            //        //                    Type = foundItem.Type
            //        //                });

            //        //                objLocalConfig.objLogging.Add_DictEntry("object_id_" + fieldCount, objDBLevel1.Objects1.First().GUID);
            //        //                objLocalConfig.objLogging.Add_DictEntry("object_name_" + fieldCount, objDBLevel1.Objects1.First().Name);
            //        //                objLocalConfig.objLogging.Add_DictEntry("object_id_parent_" + fieldCount, objDBLevel1.Objects1.First().GUID_Parent);
            //        //                objLocalConfig.objLogging.Add_DictEntry("object_operation_" + fieldCount, "exist");
            //        //            }

            //        //        }





            //        //    }
            //        //    else if (objOItem_Of_FieldToItem.Type == objLocalConfig.Globals.Type_AttributeType)
            //        //    {
            //        //        var objRel_Attributes = objDataWork_Import.Get_Related_Attributes(objOList_Fields,
            //        //            objOItem_Of_FieldToItem,
            //        //            objDoc.Dict.ContainsKey(objFields.First().Name) ? objDoc.Dict[objFields.First().Name] : null );

            //        //        objLocalConfig.objLogging.Add_DictEntry("attribute_" + fieldCount, objOItem_Of_FieldToItem.Name);
            //        //        foreach (var objORel in objRel_Attributes)
            //        //        {
            //        //            objOItem_Result = objTransaction.do_Transaction(objORel, true);


            //        //            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID)
            //        //            {
            //        //                objLocalConfig.objLogging.Add_DictEntry("error", "Attribute");
            //        //                break;
            //        //            }
            //        //        }
            //        //    }

            //        //}

            //        //var objORel_Objects = objDataWork_Import.Get_Related(objOList_Fields);
            //        //objLocalConfig.objLogging.Add_DictEntry("relations_" + fieldCount, objORel_Objects.Count);
            //        //if (objORel_Objects.Any())
            //        //{

            //        //    foreach (var objORel in objORel_Objects)
            //        //    {
            //        //        objOItem_Result = objTransaction.do_Transaction(objORel, true);
            //        //        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID)
            //        //        {
            //        //            objLocalConfig.objLogging.Add_DictEntry("error", "Relation");
            //        //            break;
            //        //        }
            //        //    }
            //        //}

            //        objLocalConfig.objLogging.Finish_Document();


            //    }

            //    objLocalConfig.objLogging.Flush_Documents();
            //}
            //else
            //{
            //    objOItem_Result = objLocalConfig.Globals.LState_Error.Clone();
            //}


            //}




            return objOItem_Result;
        }

        private void Initialize()
        {
            objDataWork_Import = new clsDataWork_Import(objLocalConfig);

            objTransaction = new clsTransaction(objLocalConfig.Globals);
            objRelationConfig = new clsRelationConfig(objLocalConfig.Globals);

            objDBLevel1 = new OntologyModDBConnector(objLocalConfig.Globals);

            objLogging = new clsLogging(objLocalConfig.Globals);
        }

    }
}
