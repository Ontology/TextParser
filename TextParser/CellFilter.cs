﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TextParser
{
    public class CellFilter
    {
        private string search;
        private object locker = new object();
        
        public string Search
        {
            get { return search; }
            set
            {
                lock(locker)
                {
                    search = value;
                    CheckRegEx();
                }
                
                
            }
        }

        private bool isRegex;
        public bool IsRegex
        {
            get { return isRegex; }
            set
            {
                lock (locker)
                {
                    isRegex = value;
                    CheckRegEx();
                }

            }
        }

        public Color MarkColor { get; set; }

        private void CheckRegEx()
        {
            RegexItem = null;

            if (IsRegex)
            {
                try
                {
                    RegexItem = new Regex(search);
                }
                catch (Exception ex) { }
            }
        }

        public Regex RegexItem { get; private set; }
            
        

        public bool IsFound(string value)
        {
            if (RegexItem != null)
            {
                return RegexItem.Match(value).Success;
            }
            else
            {
                return value.ToLower().Contains(search.ToLower());
            }
            
        }
    }
}
