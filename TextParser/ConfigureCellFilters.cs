﻿using Structure_Module;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextParser
{
    public delegate void ConfigureCellFiltersChanged(ConfigureCellFilters senderForm);
    
    public partial class ConfigureCellFilters : Form
    {
        public event ConfigureCellFiltersChanged cellFiltersChanged;

        public SortableBindingList<CellFilter> CellFilters { get; private set; }
        public ConfigureCellFilters(List<CellFilter> cellFilters)
        {
            InitializeComponent();

            CellFilters = new SortableBindingList<CellFilter>(cellFilters ?? new List<CellFilter>());
            dataGridView_CellFilters.DataSource = CellFilters;
            dataGridView_CellFilters.Columns["MarkColor"].ReadOnly = true;
            dataGridView_CellFilters.Columns["RegexItem"].Visible = false;
        }

        private void toolStripButton_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView_CellFilters_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView_CellFilters.Rows[e.RowIndex].Cells[e.ColumnIndex].Value is Color)
            {
                var color = (Color)dataGridView_CellFilters.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                colorDialog_MarkColor.Color = color;
                if (colorDialog_MarkColor.ShowDialog(this) == DialogResult.OK)
                {
                    dataGridView_CellFilters.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = colorDialog_MarkColor.Color;
                }
            }
        }

        private void dataGridView_CellFilters_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.Value != null && e.Value is Color)
            {
                e.CellStyle.BackColor = (Color)e.Value;
            }
        }

        private void dataGridView_CellFilters_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView_CellFilters_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            var cellFilterItem = (CellFilter)dataGridView_CellFilters.Rows[e.RowIndex].DataBoundItem;

            if (cellFilterItem != null)
            {
                if (string.IsNullOrEmpty(cellFilterItem.Search))
                {
                    e.Cancel = true;
                    return;
                }

                if (cellFilterItem.MarkColor == null)
                {
                    e.Cancel = true;
                    return;
                }

                if (cellFilterItem.IsRegex && cellFilterItem.RegexItem == null)
                {
                    e.Cancel = true;
                    return;
                }
                
            }
                
        }

        private void dataGridView_CellFilters_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (cellFiltersChanged != null)
            {
                cellFiltersChanged(this);
            }
        }
    }
}
