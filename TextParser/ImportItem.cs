﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParser
{
    public class ImportItem
    {
        private clsLocalConfig localConfig;
        private clsRelationConfig relationConfig;

        private OntologyModDBConnector dbReader_Items;

        public string Id_ImportItem { get; set; }
        public string Name_ImportItem { get; set; }
        public string Id_Attribute_UseDocId { get; set; }
        public bool UseDocId { get; set; }
        public string Id_Attribute_Identification { get; set; }
        public bool IdentificationItem { get; set; }
        public long OrderId { get; set; }
        public clsField FieldItem { get; set; }
        public string Id_OItem {get; set;}
        public string Name_OItem { get; set; }
        public string Id_RefItem { get; set; }
        public string Id_Parent_RefItem { get; set; }
        public string Name_RefItem { get; set; }
        public string Type_RefItem { get; set; }
        public string Id_RelationType { get; set; }
        public clsOntologyItem Direction { get; set; }

        private clsOntologyItem oItemFieldToItem = null;
        public clsOntologyItem OItemFieldToItem
        {
            get
            {
                if (oItemFieldToItem == null && !string.IsNullOrEmpty(Id_ImportItem) && !string.IsNullOrEmpty(Name_ImportItem))
                {
                    oItemFieldToItem = new clsOntologyItem
                    {
                        GUID = Id_ImportItem,
                        Name = Name_ImportItem,
                        GUID_Parent = localConfig.OItem_class_fieldtoitem.GUID,
                        Type = localConfig.Globals.Type_Object
                    };
                }

                return oItemFieldToItem;
            }
        }

        public List<clsOntologyItem> GetEntityItems(string identification)
        {
            if (string.IsNullOrEmpty(identification)) return null;
            if (string.IsNullOrEmpty(Type_RefItem) || string.IsNullOrEmpty(Id_RefItem)) return null;
            if (Type_RefItem != localConfig.Globals.Type_Class) return null;

            if (identification.Length > 255)
            {
                identification = identification.Substring(0, 254);
            }

            var searchEntityItems = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID = UseDocId ? identification : null,
                    GUID_Parent = Id_RefItem,
                    Name = !UseDocId ? identification : null
                }
            };

            var result = dbReader_Items.GetDataObjects(searchEntityItems);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            if (UseDocId)
            {
                return dbReader_Items.Objects1;
            }
            else
            {
                return dbReader_Items.Objects1.Where(entityItem => entityItem.Name.ToLower() == identification.ToLower()).ToList();
            }
            


        }

        public List<clsOntologyItem> GetFilteredAttributeItems(List<clsOntologyItem> entityItems, object value )
        {
            if (value == null) return entityItems;

            var strValue = value.ToString();

            var searchAttributes = entityItems.Select(entityItem => new clsObjectAtt
            {
                ID_Object = entityItem.GUID,
                ID_AttributeType = Id_RefItem,
                Val_Name = strValue.Length > 255 ? strValue.Substring(0,254) : strValue
            }).ToList();

            var result = dbReader_Items.GetDataObjectAtt(searchAttributes, doIds:true);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            if (FieldItem.DataType == localConfig.OItem_object_string.GUID)
            {
                return (from entityItem in entityItems
                        join objAtt in dbReader_Items.ObjAttsId on entityItem.GUID equals objAtt.ID_Object
                        select entityItem).ToList();
            }
            else
            {
                return (from entityItem in entityItems
                        join objAtt in dbReader_Items.ObjAttsId on entityItem.GUID equals objAtt.ID_Object
                        where objAtt.Val_Name == value.ToString()
                        select entityItem).ToList();
            }
            
        }

        public clsOntologyItem SaveAttributes(clsOntologyItem entityItem, object value, clsTransaction transactionManager)
        {
            if (Type_RefItem != localConfig.Globals.Type_AttributeType) return localConfig.Globals.LState_Error.Clone();
            if (string.IsNullOrEmpty(Id_RefItem)) return localConfig.Globals.LState_Error.Clone();

            var saveAtt = relationConfig.Rel_ObjectAttribute(entityItem, new clsOntologyItem { GUID = Id_RefItem, Name = Name_RefItem, GUID_Parent = Id_Parent_RefItem, Type = localConfig.Globals.Type_AttributeType }, value);

            return transactionManager.do_Transaction(saveAtt, boolRemoveAll: true);
            
        }

        public clsOntologyItem SaveRelated(clsOntologyItem entityItem, string relName, clsTransaction transactionManager)
        {
            if (entityItem == null) return localConfig.Globals.LState_Error.Clone();
            if (Type_RefItem != localConfig.Globals.Type_Class) return localConfig.Globals.LState_Error.Clone();
            if (string.IsNullOrEmpty(Id_RefItem)) return localConfig.Globals.LState_Error.Clone();
            if (string.IsNullOrEmpty(relName)) return localConfig.Globals.LState_Success.Clone();
            if (Direction == null) return localConfig.Globals.LState_Error.Clone();
            if (string.IsNullOrEmpty(Id_RelationType)) return localConfig.Globals.LState_Error.Clone();

            if (relName.Length > 255)
            {
                relName = relName.Substring(0, 254);
            }

            var searchRelatedItems = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = Id_RefItem,
                    Name = relName
                }
            };

            var result = dbReader_Items.GetDataObjects(searchRelatedItems);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            var foundItem = dbReader_Items.Objects1.FirstOrDefault(relItem => relItem.Name == relName);

            if (foundItem == null)
            {
                foundItem = new clsOntologyItem
                {
                    GUID = localConfig.Globals.NewGUID,
                    Name = relName,
                    GUID_Parent = Id_RefItem,
                    Type = localConfig.Globals.Type_Object
                };

                result = transactionManager.do_Transaction(foundItem);

                if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;
            }

            
            if (Direction.GUID == localConfig.Globals.Direction_LeftRight.GUID)
            {
                var saveRel = relationConfig.Rel_ObjectRelation(entityItem, foundItem, new clsOntologyItem { GUID = Id_RelationType });
                if (saveRel == null) return localConfig.Globals.LState_Error.Clone();

                result = transactionManager.do_Transaction(saveRel, boolRemoveAll: true);
            }
            else
            {
                var saveRel = relationConfig.Rel_ObjectRelation(foundItem, entityItem, new clsOntologyItem { GUID = Id_RelationType });
                if (saveRel == null) return localConfig.Globals.LState_Error.Clone();
                result = transactionManager.do_Transaction(saveRel, boolRemoveAll: true);
            }

            return result;
        }

        

        public List<clsOntologyItem> GetFilteredRelated(List<clsOntologyItem> entityItems, string relName)
        {
            if (relName == null) return entityItems;

            List<clsObjectRel> searchRelated = null;

            if (Direction.GUID == localConfig.Globals.Direction_LeftRight.GUID)
            {
                searchRelated = entityItems.Select(entityItem => new clsObjectRel
                {
                    ID_Object = entityItem.GUID,
                    ID_RelationType = Id_RelationType,
                    ID_Parent_Other = Id_RefItem
                }).ToList();
            }
            else
            {
                searchRelated = entityItems.Select(entityItem => new clsObjectRel
                {
                    ID_Other = entityItem.GUID,
                    ID_RelationType = Id_RelationType,
                    ID_Parent_Object = Id_RefItem
                }).ToList();
            }

            var result = dbReader_Items.GetDataObjectRel(searchRelated, doIds: true);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            if (Direction.GUID == localConfig.Globals.Direction_LeftRight.GUID)
            {
                return (from entityItem in entityItems
                        join relItem in dbReader_Items.ObjectRels on entityItem.GUID equals relItem.ID_Object
                        select entityItem).ToList();
            }
            else
            {
                return (from entityItem in entityItems
                        join relItem in dbReader_Items.ObjectRels on entityItem.GUID equals relItem.ID_Other
                        select entityItem).ToList();
            }
        }

        public ImportItem(clsLocalConfig localConfig, clsRelationConfig relationConfig)
        {
            this.localConfig = localConfig;
            this.relationConfig = relationConfig;
            Initialize();
        }


        private void Initialize()
        {
            dbReader_Items = new OntologyModDBConnector(localConfig.Globals);
        }


    }
}
