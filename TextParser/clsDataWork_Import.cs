﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;
using Ontology_Module;
using ElasticSearchConfig_Module;
using OntologyAppDBConnector;

namespace TextParser
{
    public class clsDataWork_Import
    {
        private clsLocalConfig objLocalConfig;

        private OntologyModDBConnector objDBLevel_TextParser_To_Ontology;
        private OntologyModDBConnector objDBLevel_TextParser_To_FieldToItem;
        private OntologyModDBConnector objDBLevel_FieldToItem_To_Field;
        private OntologyModDBConnector objDBLevel_FieldToItem_To_OItem;
        private OntologyModDBConnector objDBLevel_TextParser_To_FieldExtractor;
        private OntologyModDBConnector objDBLevel_FieldExtractor_To_Field;
        private OntologyModDBConnector objDBLevel_FieldToItem_Att;

        private clsRelationConfig objRelationConfig;

        private clsDataWork_Ontologies objDataWork_Ontologies;

        private clsDataWork_Index objDataWork_Index;

        private clsOntologyItem objOItem_TextParser;

        public clsOntologyItem OItem_Ontology { get; set; }

        public List<clsOntologyJoins> OList_OntologyJoins { get; set; }

        public clsOntologyItem OItem_Index
        {
            get { return objDataWork_Index.OItem_Index; }
        }

        public clsOntologyItem OItem_Server
        {
            get { return objDataWork_Index.OItem_Server; }
        }

        public clsOntologyItem OItem_Port
        {
            get { return objDataWork_Index.OItem_Port; }
        }

        public clsOntologyItem OItem_Type
        {
            get { return objDataWork_Index.OItem_Type; }
        }

        private List<ImportItem> fieldItems;
        public List<ImportItem> FieldItems
        {
            get { return fieldItems; }
            set
            {
                fieldItems = value;
            }
        }

        public List<clsObjectAtt> Get_Related_Attributes(List<clsOntologyItem> OList_Items, clsOntologyItem OItem_AttributeType, object value)
        {
            var objORel_Attributes = (from objOJoin in OList_OntologyJoins
                                      join objOItem in OList_Items on objOJoin.ID_OItem1 equals objOItem.GUID_Parent
                                      where objOJoin.ID_OItem2 == OItem_AttributeType.GUID
                                      select objRelationConfig.Rel_ObjectAttribute(objOItem, OItem_AttributeType, value,doStandardValue:true)).ToList();

            return objORel_Attributes;
        }

        public List<clsObjectRel> Get_Related(List<clsOntologyItem> OList_Items)
        {
            var objORel_LeftRight = (from objOJoin in OList_OntologyJoins
                                     join objOItem1 in OList_Items on objOJoin.ID_OItem1 equals objOItem1.GUID_Parent
                                     join objOItem2 in OList_Items on objOJoin.ID_OItem2 equals objOItem2.GUID_Parent
                                     select objRelationConfig.Rel_ObjectRelation(objOItem1, 
                                     objOItem2, 
                                     new clsOntologyItem {GUID = objOJoin.ID_OItem3,
                                     Name = objOJoin.Name_OItem3,
                                     Type = objLocalConfig.Globals.Type_RelationType})).ToList();


            return objORel_LeftRight;

        }

        private ImportItem Get_RelationTypeItem(ImportItem importItem)
        {
            var objOItem_OItem = objDBLevel_FieldToItem_To_OItem.ObjectRels.Where(f => f.ID_Object == importItem.Id_ImportItem && f.ID_RelationType == objLocalConfig.Globals.RelationType_belongingRelationType.GUID).Select(fi => new clsOntologyItem
            {
                GUID = fi.ID_Other,
                Name = fi.Name_Other,
                GUID_Parent = fi.ID_Parent_Other,
                Type = fi.Ontology
            }).FirstOrDefault();

            if (objOItem_OItem != null)
            {
                var refItem = objDataWork_Ontologies.OList_RefsOfOntologyItems.FirstOrDefault(oi => oi.ID_OntologyItem == objOItem_OItem.GUID);

                if (refItem != null)
                {
                    importItem.Id_RelationType = refItem.ID_Ref;
                }
            }

            return importItem;
        }

        private ImportItem Get_OItemOfFieldToItem(ImportItem importItem)
        {

            

            var objOItem_OItem = objDBLevel_FieldToItem_To_OItem.ObjectRels.Where(f => f.ID_Object == importItem.Id_ImportItem && f.ID_RelationType == objLocalConfig.OItem_relationtype_import_to.GUID).Select(fi => new clsOntologyItem
            {
                GUID = fi.ID_Other,
                Name = fi.Name_Other,
                GUID_Parent = fi.ID_Parent_Other,
                GUID_Relation = fi.ID_RelationType,
                Type = fi.Ontology
            }).First();

            importItem.Id_OItem = objOItem_OItem.GUID;
            importItem.Name_OItem = objOItem_OItem.Name;

            var objOList_ORef = objDataWork_Ontologies.OList_RefsOfOntologyItems.Where(oi => oi.ID_OntologyItem == importItem.Id_OItem).ToList();

            var objOList_OItem = OList_OntologyJoins.Where(oj => oj.ID_OItem1 == objOList_ORef.First().ID_Ref).ToList();

            if (objOList_OItem.Any())
            {

                importItem.Id_RefItem = objOList_OItem.First().ID_OItem1;
                importItem.Name_RefItem = objOList_OItem.First().Name_OItem1;
                importItem.Id_Parent_RefItem = objOList_OItem.First().ID_ParentOItem1;
                importItem.Type_RefItem = objOList_OItem.First().Ontology_OItem1;

                var ontologyJoin = OList_OntologyJoins.FirstOrDefault(oj => oj.ID_Join == objOList_OItem.First().ID_Join);

                if (ontologyJoin != null)
                {
                    if (ontologyJoin.Ontology_OItem3 != null && ontologyJoin.Ontology_OItem3 == objLocalConfig.Globals.Type_RelationType)
                    {
                        importItem.Id_RelationType = ontologyJoin.ID_OItem3;
                        importItem.Direction = objLocalConfig.Globals.Direction_RightLeft;
                    }
                }
                
                return importItem;
            }
            else
            {
                objOList_OItem = OList_OntologyJoins.Where(oj => oj.ID_OItem2 == objOList_ORef.First().ID_Ref).ToList();


                if (objOList_OItem.Any())
                {
                    importItem.Id_RefItem = objOList_OItem.First().ID_OItem2;
                    importItem.Name_RefItem = objOList_OItem.First().Name_OItem2;
                    importItem.Id_Parent_RefItem = objOList_OItem.First().ID_ParentOItem2;
                    importItem.Type_RefItem = objOList_OItem.First().Ontology_OItem2;

                    var ontologyJoin = OList_OntologyJoins.FirstOrDefault(oj => oj.ID_Join == objOList_OItem.First().ID_Join);

                    if (ontologyJoin != null)
                    {
                        if (ontologyJoin.Ontology_OItem3 != null && ontologyJoin.Ontology_OItem3 == objLocalConfig.Globals.Type_RelationType)
                        {
                            importItem.Id_RelationType = ontologyJoin.ID_OItem3;
                            importItem.Direction = objLocalConfig.Globals.Direction_LeftRight;
                        }
                    }

                    return importItem;
                }
                else
                {
                    objOList_OItem = OList_OntologyJoins.Where(oj => oj.ID_OItem3 == objOList_ORef.First().ID_Ref).ToList();

                    if (objOList_OItem.Any())
                    {
                        importItem.Id_RefItem = objOList_OItem.First().ID_OItem3;
                        importItem.Name_RefItem = objOList_OItem.First().Name_OItem3;
                        importItem.Id_Parent_RefItem = objOList_OItem.First().ID_ParentOItem3;
                        importItem.Type_RefItem = objOList_OItem.First().Ontology_OItem3;

                        
                        return importItem;
                    }
                }
            }

            return null;
        }

        public List<clsOntologyItem> Get_FieldsOfFieldToItem(clsOntologyItem OItem_FieldToItem)
        {
            return objDBLevel_FieldToItem_To_Field.ObjectRels.Where(f => f.ID_Object == OItem_FieldToItem.GUID)
                .OrderBy(f => f.OrderID)
                .Select(f => new clsOntologyItem {GUID = f.ID_Other,
                    Name = f.Name_Other,
                    GUID_Parent = f.ID_Parent_Other,
                    Type = objLocalConfig.Globals.Type_Object
                }).ToList();
        }

        public List<clsOntologyJoins> OList_Joins
        {
            get { return objDataWork_Ontologies.OList_OntologyJoins.Where(j => j.ID_Ontology == OItem_Ontology.GUID).ToList(); }
        }

        public clsOntologyItem GetData(clsOntologyItem OItem_TextParser, List<clsField> fieldList)
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            objOItem_TextParser = OItem_TextParser;

            objOItem_Result = GetSubData001_OntologyOfTextParser();
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objOItem_Result = GetSubData002_OntologyData();
                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    objOItem_Result = GetSubData003_FieldToItemsOfTextParser();
                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        objOItem_Result = GetSubData004_FieldsOfFieldToItems();

                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            objOItem_Result = GetSubData005_OItemsOfFieldToItems();

                            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                            {
                                objOItem_Result = GetSubData006_TextParser_To_FieldExtractor();

                                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                {
                                    objOItem_Result = GetSubData007_FieldExtractor_To_Fields();

                                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                    {
                                        objOItem_Result = objDataWork_Index.GetIndexData_OfRef(objOItem_TextParser, objLocalConfig.OItem_relationtype_located_at, objLocalConfig.OItem_relationtype_belonging,  objLocalConfig.Globals.Direction_LeftRight);

                                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                        {
                                            objOItem_Result = GetSubData008_FieldToItem_Atts();
                                            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                            {
                                                FieldItems = (from fieldToItem in objDBLevel_TextParser_To_FieldToItem.ObjectRels
                                                              join fieldRef in objDBLevel_FieldToItem_To_Field.ObjectRels on fieldToItem.ID_Other equals fieldRef.ID_Object
                                                              join fieldItem in fieldList on fieldRef.ID_Other equals fieldItem.ID_Field
                                                              join useDocId in objDBLevel_FieldToItem_Att.ObjAtts.Where(objAtt => objAtt.ID_AttributeType == objLocalConfig.OItem_attributetype_use_doc_id.GUID) on fieldToItem.ID_Other equals useDocId.ID_Object into useDocIds
                                                              from useDocId in useDocIds.DefaultIfEmpty()
                                                              join identificator in objDBLevel_FieldToItem_Att.ObjAtts.Where(objAtt => objAtt.ID_AttributeType == objLocalConfig.OItem_attributetype_identification.GUID) on fieldToItem.ID_Other equals identificator.ID_Object into identifications
                                                              from identificator in identifications.DefaultIfEmpty()
                                                              select new ImportItem(objLocalConfig, objRelationConfig)
                                                              {
                                                                  Id_ImportItem = fieldToItem.ID_Other,
                                                                  Name_ImportItem = fieldToItem.Name_Other,
                                                                  Id_Attribute_Identification = identificator != null ? identificator.ID_Attribute : null,
                                                                  IdentificationItem = identificator != null ? identificator.Val_Bit != null ? identificator.Val_Bit.Value : false : false,
                                                                  Id_Attribute_UseDocId = useDocId != null ? useDocId.ID_Attribute : null,
                                                                  UseDocId = useDocId != null ? useDocId.Val_Bit != null ? useDocId.Val_Bit.Value : false : false,
                                                                  FieldItem = fieldItem,
                                                                  OrderId = fieldToItem.OrderID.Value
                                                              }).ToList();

                                                FieldItems = FieldItems.Select(fieldItem =>
                                                {
                                                    var fieldItemExtend = Get_OItemOfFieldToItem(fieldItem);
                                                    if (fieldItemExtend == null) return null;

                                                    fieldItemExtend = Get_RelationTypeItem(fieldItemExtend);

                                                    return fieldItemExtend;
                                                }).Where(fieldItem => fieldItem != null).ToList();

                                                //FieldList = objDBLevel_FieldExtractor_To_Field.ObjectRels.Select(f => new clsOntologyItem
                                                //{
                                                //    GUID = f.ID_Other,
                                                //    Name = f.Name_Other,
                                                //    GUID_Parent = objLocalConfig.OItem_class_field.GUID,
                                                //    Type = objLocalConfig.Globals.Type_Object
                                                //}).ToList();
                                            }
                                                
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }

            return objOItem_Result;
        }

        private clsOntologyItem GetSubData001_OntologyOfTextParser()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var objORel_TextParser_To_Ontology = new List<clsObjectRel> { new clsObjectRel { ID_Object = objOItem_TextParser.GUID,
                ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                ID_Parent_Other = objLocalConfig.Globals.Class_Ontologies.GUID } };

            objOItem_Result = objDBLevel_TextParser_To_Ontology.GetDataObjectRel(objORel_TextParser_To_Ontology, doIds: false);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (objDBLevel_TextParser_To_Ontology.ObjectRels.Any())
                {
                    OItem_Ontology = objDBLevel_TextParser_To_Ontology.ObjectRels.Select(o => new clsOntologyItem
                    {
                        GUID = o.ID_Other,
                        Name = o.Name_Other,
                        GUID_Parent = o.ID_Parent_Other,
                        Type = objLocalConfig.Globals.Type_Object
                    }).ToList().First();


                }
                else
                {
                    objOItem_Result = objLocalConfig.Globals.LState_Error.Clone();
                }
            }

            return objOItem_Result;
        }

        private clsOntologyItem GetSubData002_OntologyData()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            objOItem_Result = objDataWork_Ontologies.GetData_BaseData();

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                OList_OntologyJoins = objDataWork_Ontologies.OList_OntologyJoins.Where(o => o.ID_Ontology == OItem_Ontology.GUID).ToList();

            }

            return objOItem_Result;
        }

        private clsOntologyItem GetSubData003_FieldToItemsOfTextParser()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var objORel_FieldToItemsOfTextParser = new List<clsObjectRel> {new clsObjectRel {ID_Object = objOItem_TextParser.GUID,
                ID_RelationType = objLocalConfig.OItem_relationtype_contains.GUID,
                ID_Parent_Other = objLocalConfig.OItem_class_fieldtoitem.GUID } };

            objOItem_Result = objDBLevel_TextParser_To_FieldToItem.GetDataObjectRel(objORel_FieldToItemsOfTextParser, doIds: false);

            return objOItem_Result;
        }

        private clsOntologyItem GetSubData004_FieldsOfFieldToItems()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var objORel_FieltToItems_To_Fields = objDBLevel_TextParser_To_FieldToItem.ObjectRels.Select(f => new clsObjectRel
            {
                ID_Object = f.ID_Other,
                ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                ID_Parent_Other = objLocalConfig.OItem_class_field.GUID
            }).ToList();


            objOItem_Result = objDBLevel_FieldToItem_To_Field.GetDataObjectRel(objORel_FieltToItems_To_Fields, doIds: false);


            return objOItem_Result;
        }

        private clsOntologyItem GetSubData005_OItemsOfFieldToItems()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var objORel_FieldToItems_To_OItems = objDBLevel_TextParser_To_FieldToItem.ObjectRels.Select(o => new clsObjectRel
            {
                ID_Object = o.ID_Other,
                ID_RelationType = objLocalConfig.OItem_relationtype_import_to.GUID,
                ID_Parent_Other = objLocalConfig.Globals.Class_OntologyItems.GUID
            }).ToList();

            objORel_FieldToItems_To_OItems.AddRange(objDBLevel_TextParser_To_FieldToItem.ObjectRels.Select(o => new clsObjectRel
            {
                ID_Object = o.ID_Other,
                ID_RelationType = objLocalConfig.Globals.RelationType_belongingRelationType.GUID,
                ID_Parent_Other = objLocalConfig.Globals.Class_OntologyItems.GUID
            }));

            objOItem_Result = objDBLevel_FieldToItem_To_OItem.GetDataObjectRel(objORel_FieldToItems_To_OItems, doIds: false);

            return objOItem_Result;
        }

        private clsOntologyItem GetSubData006_TextParser_To_FieldExtractor()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var objORel_TextParser_To_FieldExtractor = new List<clsObjectRel> {new clsObjectRel { ID_Object = objOItem_TextParser.GUID,
                ID_RelationType = objLocalConfig.OItem_relationtype_todo.GUID,
                ID_Parent_Other = objLocalConfig.OItem_class_field_extractor_parser.GUID  } };

            objOItem_Result = objDBLevel_TextParser_To_FieldExtractor.GetDataObjectRel(objORel_TextParser_To_FieldExtractor, doIds: false);

            return objOItem_Result;
        }

        private clsOntologyItem GetSubData007_FieldExtractor_To_Fields()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var objORel_FieldExtractor_To_Fields = objDBLevel_TextParser_To_FieldExtractor.ObjectRels.Select(tp => new clsObjectRel
            {
                ID_Object = tp.ID_Other,
                ID_RelationType = objLocalConfig.OItem_relationtype_entry.GUID,
                ID_Parent_Other = objLocalConfig.OItem_class_field.GUID
            }).ToList();


            objOItem_Result = objDBLevel_FieldExtractor_To_Field.GetDataObjectRel(objORel_FieldExtractor_To_Fields, doIds: false);


            return objOItem_Result;
        }

        private clsOntologyItem GetSubData008_FieldToItem_Atts()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var searchFieldToItem = objDBLevel_TextParser_To_FieldToItem.ObjectRels.Select(fieldToItem => new clsObjectAtt
            {
                ID_Object = fieldToItem.ID_Other,
                ID_AttributeType = objLocalConfig.OItem_attributetype_identification.GUID
            }).ToList();

            searchFieldToItem.AddRange(objDBLevel_TextParser_To_FieldToItem.ObjectRels.Select(fieldToItem => new clsObjectAtt
            {
                ID_Object = fieldToItem.ID_Other,
                ID_AttributeType = objLocalConfig.OItem_attributetype_use_doc_id.GUID
            }));

            objOItem_Result = objDBLevel_FieldToItem_Att.GetDataObjectAtt(searchFieldToItem);

            return objOItem_Result;
        }

        public clsDataWork_Import(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;

            Initialize();
        }

        private void Initialize()
        {
            objDataWork_Ontologies = new clsDataWork_Ontologies(objLocalConfig.Globals);

            objDBLevel_TextParser_To_Ontology = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_TextParser_To_FieldToItem = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_FieldToItem_To_Field = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_FieldToItem_To_OItem = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_TextParser_To_FieldExtractor = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_FieldExtractor_To_Field = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_FieldToItem_Att = new OntologyModDBConnector(objLocalConfig.Globals);
            objDataWork_Index = new clsDataWork_Index(objLocalConfig.Globals);
            

            objRelationConfig = new clsRelationConfig(objLocalConfig.Globals);
        }
    }
}
