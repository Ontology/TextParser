﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;
using Ontology_Module;
using OntologyAppDBConnector;

namespace TextParser
{
    public class clsDataWork_FieldParser
    {
        private OntologyModDBConnector objDBLevel_FieldToRegEx;
        private OntologyModDBConnector objDBLevel_RegEx__Pattern;

        private OntologyModDBConnector objDBLevel_FieldParser_To_Field;
        private OntologyModDBConnector objDBLevel_Fields;
        private OntologyModDBConnector objDBLevel_Fields_Rel;
        private OntologyModDBConnector objDBLevel_Fields_Att;
        private OntologyModDBConnector objDBLevel_Filter_Att;
        private OntologyModDBConnector objDBLevel_RegEx_Att;
        private OntologyModDBConnector objDBLevel_ReplaceWith;
        private OntologyModDBConnector objDBLevel_UserContentOfField;
        private OntologyModDBConnector objDBLevel_DoAll;
        private OntologyModDBConnector objDBLevel_RegExReplace;
        private OntologyModDBConnector objDBLevel_RegExOfReplace;
        private OntologyModDBConnector objDBLevel_RegExRegEx;
        private OntologyModDBConnector objDBLevel_RegExReplaceWith;
        private OntologyModDBConnector objDBLevel_ContainedFields;

        private OntologyModDBConnector objDBLevel_MergedFields;
        private OntologyModDBConnector objDBLevel_MergedFieldsRef;

        private OntologyModDBConnector objDBLevel_DocumentItem;
        private OntologyModDBConnector objDBLevel_DocItemToField;

        private clsTransaction objTransaction;
        private clsRelationConfig objRelationConfig;
      
        private clsLocalConfig objLocalConfig;

        public List<clsReplaceList> ReplaceList { get; set; } 
        public List<clsField> FieldList { get; set; } 

        public clsDataWork_FieldParser(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;
            Initialize();
        }

        public List<clsDocumentItem> GetDockumentItemsWithFields(clsOntologyItem oItem_TextParser)
        {
            var documentItems = new List<clsDocumentItem>();

            var searchItems = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItem_TextParser.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_contains.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_class_documentitem.GUID
                }

            };

            var result = objDBLevel_DocumentItem.GetDataObjectRel(searchItems, doIds: false);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var searchFields = objDBLevel_DocumentItem.ObjectRels.Select(docItem => new clsObjectRel
                {
                    ID_Object = docItem.ID_Other,
                    ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_class_field.GUID
                }).ToList();

                result = objDBLevel_DocItemToField.GetDataObjectRel(searchFields, doIds: false);

                if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    documentItems = (from docItem in objDBLevel_DocumentItem.ObjectRels
                        join fieldItem in objDBLevel_DocItemToField.ObjectRels on docItem.ID_Other equals
                            fieldItem.ID_Object
                        select new clsDocumentItem
                        {
                            ID_DocItem = docItem.ID_Other,
                            Name_DocItem = docItem.Name_Other,
                            ID_Field = fieldItem.ID_Other,
                            Name_Field = fieldItem.Name_Other
                        }).ToList();
                    
                }
                else
                {
                    documentItems = null;
                }
                
            }
            else
            {
                documentItems = null;
            }

            return documentItems;
        }

        public List<clsOntologyItem> GetDocumentItems(clsOntologyItem oItem_TextParser)
        {
            var documentItems = new List<clsOntologyItem>();

            var searchItems = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItem_TextParser.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_contains.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_class_documentitem.GUID
                }

            };

            var result = objDBLevel_DocumentItem.GetDataObjectRel(searchItems, doIds: false);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                documentItems = objDBLevel_DocumentItem.ObjectRels.Select(docItem => new clsOntologyItem
                {
                    GUID = docItem.ID_Other,
                    Name = docItem.Name_Other,
                    GUID_Parent = docItem.ID_Parent_Other,
                    Type = objLocalConfig.Globals.Type_Object
                }).ToList();
            }
            else
            {
                documentItems = null;
            }

            return documentItems;
        }

        public clsOntologyItem GetDocumentItem(object value, string idDoc, clsOntologyItem oItem_TextParser, clsOntologyItem oItem_Field)
        {

            var name = idDoc;

            var objOItem_DocumentItem = new clsOntologyItem
            {
                Name = name,
                GUID_Parent = objLocalConfig.OItem_class_documentitem.GUID,
                Type = objLocalConfig.Globals.Type_Object
            };

            var searchObjRel = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItem_TextParser.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_class_documentitem.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_contains.GUID
                }
            };

            var result = objDBLevel_DocumentItem.GetDataObjectRel(searchObjRel, doIds: false);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var searchDocItemToField = objDBLevel_DocumentItem.ObjectRels.Where(docItem => docItem.Name_Other == idDoc).Select(docItem => new clsObjectRel
                {
                    ID_Object = docItem.ID_Other,
                    ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                    ID_Other = oItem_Field.GUID
                }).ToList();

                if (searchDocItemToField.Any())
                {
                    result = objDBLevel_DocItemToField.GetDataObjectRel(searchDocItemToField, doIds: false);
                    if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        if (objDBLevel_DocItemToField.ObjectRels.Any())
                        {
                            objOItem_DocumentItem = new clsOntologyItem
                            {
                                GUID = objDBLevel_DocItemToField.ObjectRels.First().ID_Object,
                                Name =  objDBLevel_DocItemToField.ObjectRels.First().Name_Object,
                                GUID_Parent = objDBLevel_DocItemToField.ObjectRels.First().ID_Parent_Object,
                                Type = objLocalConfig.Globals.Type_Object
                            };

                            return objOItem_DocumentItem;
                        }
                        else
                        {
                            return CreateDocumentItem(value, idDoc, oItem_TextParser, oItem_Field);
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return CreateDocumentItem(value, idDoc, oItem_TextParser, oItem_Field);
                }

                
            }
            else
            {
                return null;
            }

            



            


        }

        public clsOntologyItem CreateDocumentItem(object value, string idDoc, clsOntologyItem oItem_TextParser, clsOntologyItem oItem_Field)
        {
            var objOItem_DocumentItem = new clsOntologyItem();
            objOItem_DocumentItem.GUID = objLocalConfig.Globals.NewGUID;
            objOItem_DocumentItem.Name = idDoc;
            objOItem_DocumentItem.GUID_Parent = objLocalConfig.OItem_class_documentitem.GUID;
            objOItem_DocumentItem.Type = objLocalConfig.Globals.Type_Object;

            objTransaction.ClearItems();

            var result = objTransaction.do_Transaction(objOItem_DocumentItem);
            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var rel_TextParserToDocumentItem = objRelationConfig.Rel_ObjectRelation(oItem_TextParser,
                    objOItem_DocumentItem, objLocalConfig.OItem_relationtype_contains);

                result = objTransaction.do_Transaction(rel_TextParserToDocumentItem);

                if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    clsObjectAtt rel_ObjectAtt;
                    if (value is bool)
                    {
                        rel_ObjectAtt = objRelationConfig.Rel_ObjectAttribute(objOItem_DocumentItem,
                            objLocalConfig.OItem_attributetype_bitvalue, value);

                    }
                    else if (value is DateTime)
                    {
                        rel_ObjectAtt = objRelationConfig.Rel_ObjectAttribute(objOItem_DocumentItem,
                            objLocalConfig.OItem_attributetype_datetimevalue, value);
                    }
                    else if (value is long)
                    {
                        rel_ObjectAtt = objRelationConfig.Rel_ObjectAttribute(objOItem_DocumentItem,
                            objLocalConfig.OItem_attributetype_longvalue, value);
                    }
                    else if (value is double)
                    {
                        rel_ObjectAtt = objRelationConfig.Rel_ObjectAttribute(objOItem_DocumentItem,
                            objLocalConfig.OItem_attributetype_doublevalue, value);
                    }
                    else if (value is string)
                    {
                        rel_ObjectAtt = objRelationConfig.Rel_ObjectAttribute(objOItem_DocumentItem,
                            objLocalConfig.OItem_attributetype_stringvalue, value);
                    }
                    else
                    {
                        return null;
                    }

                    result = objTransaction.do_Transaction(rel_ObjectAtt);

                    if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        var rel_ObjRel = objRelationConfig.Rel_ObjectRelation(objOItem_DocumentItem, oItem_Field,
                            objLocalConfig.OItem_relationtype_belongs_to);

                        result = objTransaction.do_Transaction(rel_ObjRel);
                        if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            return objOItem_DocumentItem;
                        }
                        else
                        {
                            objTransaction.rollback();
                            return null;
                        }
                    }
                    else
                    {
                        objTransaction.rollback();
                        return null;
                    }

                }
                else
                {
                    objTransaction.rollback();
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        
        public clsObjectAtt GetRegexOfField(clsOntologyItem OItem_Field, clsOntologyItem OItem_RelationType)
        {
            clsObjectAtt objOARegEx = null;

            var objORelL_Field_To_RegEx = new List<clsObjectRel>
                {
                    new clsObjectRel
                        {
                            ID_Object = OItem_Field.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_regular_expressions.GUID,
                            ID_RelationType = OItem_RelationType.GUID
                        }
                };

            var objOItem_Result = objDBLevel_FieldToRegEx.GetDataObjectRel(objORelL_Field_To_RegEx, doIds:true);
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var objOARelL_RegEx__Pattern = objDBLevel_FieldToRegEx.ObjectRelsId.Select(p => new clsObjectAtt
                {
                    ID_AttributeType = objLocalConfig.OItem_attributetype_regex.GUID,
                    ID_Object = p.ID_Other
                }).ToList();

                if (objOARelL_RegEx__Pattern.Any())
                {
                    objOItem_Result = objDBLevel_RegEx__Pattern.GetDataObjectAtt(objOARelL_RegEx__Pattern,
                                                                                   doIds: false);

                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        if (objDBLevel_RegEx__Pattern.ObjAtts.Any())
                        {
                            objOARegEx = objDBLevel_RegEx__Pattern.ObjAtts.First();
                        }
                    }

                }
            }

            return objOARegEx;
        }

        public clsOntologyItem GetPatternByPattern(string pattern)
        {
            clsOntologyItem OItem_Pattern = null;

            var objOARel_Pattern = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                        {
                            ID_AttributeType = objLocalConfig.OItem_attributetype_regex.GUID,
                            ID_Class = objLocalConfig.OItem_class_regular_expressions.GUID
                        }
                };

            var objOItem_Result = objDBLevel_RegEx__Pattern.GetDataObjectAtt(objOARel_Pattern, doIds: false);
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var objOAL_Pattern =
                    objDBLevel_RegEx__Pattern.ObjAtts.Where(p => p.Val_String == pattern).ToList();
                
                if (objOAL_Pattern.Any())
                {
                    var oList_Pattern = objOAL_Pattern.Select(p => new clsOntologyItem
                        {
                            GUID = p.ID_Object,
                            Name = p.Name_Object,
                            GUID_Parent = p.ID_Class,
                            Type = objLocalConfig.Globals.Type_Object
                        }).ToList();

                    OItem_Pattern = oList_Pattern.First();
                }
            }

            return OItem_Pattern;
        }

        private clsOntologyItem GetSubData_RegExReplace()
        {
            var result = objLocalConfig.Globals.LState_Success.Clone();

            objDBLevel_RegExOfReplace.ObjectRels.Clear();
            objDBLevel_RegExRegEx.ObjAtts.Clear();
            objDBLevel_RegExReplaceWith.ObjAtts.Clear();
            ReplaceList = new List<clsReplaceList>();

            var searchReplace = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_contains.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_class_field_replace__textparser_.GUID
                }
            };

            result = objDBLevel_RegExReplace.GetDataObjectRel(searchReplace, doIds: false);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var searchText = objDBLevel_RegExReplace.ObjectRels.Select(rep => new clsObjectAtt
                {
                    ID_AttributeType = objLocalConfig.OItem_attributetype_text.GUID,
                    ID_Object = rep.ID_Other
                }).ToList();

                if (searchText.Any())
                {
                    result = objDBLevel_RegExReplaceWith.GetDataObjectAtt(searchText, doIds: false);
                }
            }

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var searchRegEx = objDBLevel_RegExReplace.ObjectRels.Select(rep => new clsObjectRel
                {
                    ID_Object = rep.ID_Other,
                    ID_RelationType = objLocalConfig.OItem_relationtype_find.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_class_regular_expressions.GUID
                }).ToList();

                if (searchRegEx.Any())
                {
                    result = objDBLevel_RegExOfReplace.GetDataObjectRel(searchRegEx, doIds: false);

                }
            }

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var searchRegExRegEx = objDBLevel_RegExOfReplace.ObjectRels.Select(reg => new clsObjectAtt
                {
                    ID_Object = reg.ID_Other,
                    ID_AttributeType = objLocalConfig.OItem_attributetype_regex.GUID
                }).ToList();

                if (searchRegExRegEx.Any())
                {
                    result = objDBLevel_RegExRegEx.GetDataObjectAtt(searchRegExRegEx, doIds: false);
                }
            }

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var fieldList =
                    objDBLevel_RegExReplace.ObjectRels.GroupBy(field => field.ID_Object)
                        .Select(field => new clsOntologyItem {GUID = field.Key})
                        .ToList();

                fieldList.ForEach(field =>
                {
                    var replaceList = (from objFieldReplace in objDBLevel_RegExReplace.ObjectRels.OrderBy(repl => repl.OrderID)
                        where objFieldReplace.ID_Object == field.GUID
                        join objFieldText in objDBLevel_RegExReplaceWith.ObjAtts
                            on objFieldReplace.ID_Other equals objFieldText.ID_Object
                            into objFieldTexts
                        from objFieldText in objFieldTexts.DefaultIfEmpty()
                        join objRegExOfReplace in
                            objDBLevel_RegExOfReplace.ObjectRels on
                            objFieldReplace.ID_Other equals objRegExOfReplace.ID_Object
                        join objRegExOfRegEx in objDBLevel_RegExRegEx.ObjAtts on
                            objRegExOfReplace.ID_Other equals objRegExOfRegEx.ID_Object
                        select new clsRegExReplace()
                        {
                            ID_Field = field.GUID,
                            ID_FieldReplace = objFieldReplace.ID_Other,
                            Name_FieldReplace = objFieldReplace.Name_Other,
                            ID_Attribute_Text =
                                objFieldText != null ? objFieldText.ID_Attribute : null,
                            ReplaceWith =
                                objFieldText != null
                                    ? objFieldText.Val_String ?? ""
                                    : "",
                            ID_RegEx = objRegExOfReplace.ID_Other,
                            ID_Attribute_RegEx = objRegExOfRegEx.ID_Attribute,
                            RegExSearch = objRegExOfRegEx.Val_String
                        }).ToList();

                    ReplaceList.Add(new clsReplaceList
                    {
                        ID_Field = field.GUID,
                        ReplaceList = replaceList
                    });
                });
            }

            

            return result;
        }

        public clsOntologyItem GetData_FieldsOfFieldParser(clsOntologyItem OItem_Parser = null)
        {
            var objOFieldList = new List<clsOntologyItem>
                {
                    new clsOntologyItem 
                    {
                        GUID_Parent = objLocalConfig.OItem_class_field.GUID
                    }
                };

            var objOItem_Result = objDBLevel_Fields.GetDataObjects(objOFieldList);

            List<clsObjectRel> objORel_Fields_Rel = new List<clsObjectRel>();
            List<clsObjectAtt> objORel_Fields_att = new List<clsObjectAtt>();
            List<clsObjectAtt> objORel_Filter_att = new List<clsObjectAtt>();
            List<clsObjectAtt> objORel_RegEx_Att = new List<clsObjectAtt>();
            List<clsObjectRel> objORel_Replacewith = new List<clsObjectRel>();
            List<clsObjectRel> objORel_ContentField = new List<clsObjectRel>();

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                
                var objORel_FieldParser_To_Field = new List<clsObjectRel>
                {
                    new clsObjectRel
                        {
                            ID_Object = OItem_Parser != null ? OItem_Parser.GUID : null,
                            ID_Parent_Object = OItem_Parser == null ? objLocalConfig.OItem_class_field_extractor_parser.GUID : null,
                            ID_RelationType = objLocalConfig.OItem_relationtype_entry.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_field.GUID
                        },
                    new clsObjectRel
                    {
                        ID_Object = OItem_Parser != null ? OItem_Parser.GUID : null,
                            ID_Parent_Object = OItem_Parser == null ? objLocalConfig.OItem_class_field_extractor_parser.GUID : null,
                            ID_RelationType = objLocalConfig.OItem_relationtype_starts_with.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_field.GUID
                    },
                    new clsObjectRel
                    {
                        ID_Object = OItem_Parser != null ? OItem_Parser.GUID : null,
                            ID_Parent_Object = OItem_Parser == null ? objLocalConfig.OItem_class_field_extractor_parser.GUID : null,
                            ID_RelationType = objLocalConfig.OItem_relationtype_ends_with.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_field.GUID
                    }
                };

                objORel_ContentField = new List<clsObjectRel>
                        {
                            new clsObjectRel
                                {
                                    ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                                    ID_RelationType = objLocalConfig.OItem_relationtype_parsesource.GUID,
                                    ID_Parent_Other = objLocalConfig.OItem_class_field.GUID
                                }
                        };

                objOItem_Result = objDBLevel_FieldParser_To_Field.GetDataObjectRel(objORel_FieldParser_To_Field,
                                                                                     doIds: false);

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    

                    

                    if (OItem_Parser == null || !objDBLevel_FieldParser_To_Field.ObjectRels.Any())
                    {
                        

                        objORel_Fields_att = new List<clsObjectAtt>
                        {
                            new clsObjectAtt
                                {
                                    ID_AttributeType = objLocalConfig.OItem_attributetype_remove_from_source.GUID,
                                    ID_Class = objLocalConfig.OItem_class_field.GUID
                                },
                            new clsObjectAtt
                                {
                                    ID_AttributeType = objLocalConfig.OItem_attributetype_useorderid.GUID,
                                    ID_Class = objLocalConfig.OItem_class_field.GUID
                                },
                            new clsObjectAtt
                                {
                                    ID_AttributeType = objLocalConfig.OItem_attributetype_uselastvalid.GUID,
                                    ID_Class = objLocalConfig.OItem_class_field.GUID
                                },
                            new clsObjectAtt
                                {
                                    ID_AttributeType = objLocalConfig.OItem_attributetype_htmldecode.GUID,
                                    ID_Class = objLocalConfig.OItem_class_field.GUID
                                }
                        };

                        objORel_Fields_Rel = new List<clsObjectRel>
                        {
                            new clsObjectRel
                                {
                                    ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                                    ID_RelationType = objLocalConfig.OItem_relationtype_value_type.GUID,
                                    ID_Parent_Other = objLocalConfig.OItem_class_datatypes.GUID
                                },
                            new clsObjectRel
                                {
                                    ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                                    ID_RelationType = objLocalConfig.OItem_relationtype_contains.GUID,
                                    ID_Parent_Other = objLocalConfig.OItem_class_field.GUID
                                },
                            new clsObjectRel
                                {
                                    ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                                    ID_RelationType = objLocalConfig.OItem_relationtype_is.GUID,
                                    ID_Parent_Other = objLocalConfig.OItem_class_metadata__parser_.GUID
                                },
                            new clsObjectRel
                                {
                                    ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                                    ID_RelationType = objLocalConfig.OItem_relationtype_main.GUID,
                                    ID_Parent_Other = objLocalConfig.OItem_class_regex_field_filter.GUID
                                },
                            new clsObjectRel
                                {
                                    ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                                    ID_RelationType = objLocalConfig.OItem_relationtype_pre.GUID,
                                    ID_Parent_Other = objLocalConfig.OItem_class_regex_field_filter.GUID
                                },
                            new clsObjectRel
                                {
                                    ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                                    ID_RelationType = objLocalConfig.OItem_relationtype_posts.GUID,
                                    ID_Parent_Other = objLocalConfig.OItem_class_regex_field_filter.GUID
                                },
                            new clsObjectRel
                                {
                                    ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                                    ID_RelationType = objLocalConfig.OItem_relationtype_main.GUID,
                                    ID_Parent_Other = objLocalConfig.OItem_class_regular_expressions.GUID
                                },
                            new clsObjectRel
                                {
                                    ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                                    ID_RelationType = objLocalConfig.OItem_relationtype_pre.GUID,
                                    ID_Parent_Other = objLocalConfig.OItem_class_regular_expressions.GUID
                                },
                            new clsObjectRel
                                {
                                    ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                                    ID_RelationType = objLocalConfig.OItem_relationtype_posts.GUID,
                                    ID_Parent_Other = objLocalConfig.OItem_class_regular_expressions.GUID
                                },
                            new clsObjectRel
                                {
                                    ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                                    ID_RelationType = objLocalConfig.OItem_relationtype_is.GUID,
                                    ID_Parent_Other = objLocalConfig.OItem_class_csv_field.GUID
                                }
                        };


                        objORel_Filter_att = new List<clsObjectAtt>
                        {
                            new clsObjectAtt
                                {
                                    ID_AttributeType = objLocalConfig.OItem_attributetype_equal.GUID,
                                    ID_Class = objLocalConfig.OItem_class_regex_field_filter.GUID
                                },
                            new clsObjectAtt
                                {
                                    ID_AttributeType = objLocalConfig.OItem_attributetype_pattern.GUID,
                                    ID_Class = objLocalConfig.OItem_class_regex_field_filter.GUID
                                }
                        };

                        objORel_RegEx_Att = new List<clsObjectAtt>
                        {
                            new clsObjectAtt
                                {
                                    ID_AttributeType = objLocalConfig.OItem_attributetype_regex.GUID,
                                    ID_Class = objLocalConfig.OItem_class_regular_expressions.GUID
                                }
                        };

                        objORel_Replacewith = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                                ID_RelationType = objLocalConfig.OItem_relationtype_replace_with.GUID
                            }
                        };
                    }
                    else
                    {
                        objORel_Fields_att = objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectAtt
                                {
                                    ID_Object = f.ID_Other,
                                    ID_AttributeType = objLocalConfig.OItem_attributetype_remove_from_source.GUID,
                                    ID_Class = objLocalConfig.OItem_class_field.GUID
                                }).ToList();
                        objORel_Fields_att.AddRange(objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectAtt
                                {
                                    ID_Object = f.ID_Other,
                                    ID_AttributeType = objLocalConfig.OItem_attributetype_useorderid.GUID,
                                    ID_Class = objLocalConfig.OItem_class_field.GUID
                                }));
                        objORel_Fields_att.AddRange(objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectAtt
                        {
                            ID_Object = f.ID_Other,
                            ID_AttributeType = objLocalConfig.OItem_attributetype_htmldecode.GUID,
                            ID_Class = objLocalConfig.OItem_class_field.GUID
                        }));


                        objORel_Fields_Rel = objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectRel
                        {
                            ID_Object = f.ID_Other,
                            ID_RelationType = objLocalConfig.OItem_relationtype_value_type.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_datatypes.GUID
                        }).ToList();


                        objORel_Fields_Rel.AddRange(objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectRel
                        {
                            ID_Object = f.ID_Other,
                            ID_RelationType = objLocalConfig.OItem_relationtype_contains.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_field.GUID
                        }));

                        objORel_Fields_Rel.AddRange(objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectRel
                        {
                            ID_Object = f.ID_Other,
                            ID_RelationType = objLocalConfig.OItem_relationtype_is.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_metadata__parser_.GUID
                        }));

                        objORel_Fields_Rel.AddRange(objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectRel
                        {
                            ID_Object = f.ID_Other,
                            ID_RelationType = objLocalConfig.OItem_relationtype_main.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_regex_field_filter.GUID
                        }));

                        objORel_Fields_Rel.AddRange(objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectRel
                        {
                            ID_Object = f.ID_Other,
                            ID_RelationType = objLocalConfig.OItem_relationtype_pre.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_regex_field_filter.GUID
                        }));

                        objORel_Fields_Rel.AddRange(objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectRel
                        {
                            ID_Object = f.ID_Other,
                            ID_RelationType = objLocalConfig.OItem_relationtype_posts.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_regex_field_filter.GUID
                        }));

                        objORel_Fields_Rel.AddRange(objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectRel
                        {
                            ID_Object = f.ID_Other,
                            ID_RelationType = objLocalConfig.OItem_relationtype_main.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_regular_expressions.GUID
                        }));

                        objORel_Fields_Rel.AddRange(objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectRel
                        {
                            ID_Object = f.ID_Other,
                            ID_RelationType = objLocalConfig.OItem_relationtype_pre.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_regular_expressions.GUID
                        }));

                        objORel_Fields_Rel.AddRange(objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectRel
                        {
                            ID_Object = f.ID_Other,
                            ID_RelationType = objLocalConfig.OItem_relationtype_posts.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_regular_expressions.GUID
                        }));

                        objORel_Fields_Rel.AddRange(objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectRel
                        {
                            ID_Object = f.ID_Other,
                            ID_RelationType = objLocalConfig.OItem_relationtype_is.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_csv_field.GUID
                        }));

                        objORel_Filter_att = objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectAtt
                        {
                            ID_Object = f.ID_Other,
                            ID_AttributeType = objLocalConfig.OItem_attributetype_equal.GUID
                        }).ToList();


                        objORel_Replacewith = objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectRel {
                                ID_Object = f.ID_Other,
                                ID_RelationType = objLocalConfig.OItem_relationtype_replace_with.GUID
                            }).ToList();

                        objORel_RegEx_Att = new List<clsObjectAtt>
                        {
                            new clsObjectAtt
                                {
                                    ID_AttributeType = objLocalConfig.OItem_attributetype_regex.GUID,
                                    ID_Class = objLocalConfig.OItem_class_regular_expressions.GUID
                                }
                        };

                }
                
            }




                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID) return objOItem_Result;
            
               
                objOItem_Result = objDBLevel_Fields_Att.GetDataObjectAtt(objORel_Fields_att, doIds: false);

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID) return objOItem_Result;

                objOItem_Result = objDBLevel_Fields_Rel.GetDataObjectRel(objORel_Fields_Rel, doIds: false);

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID) return objOItem_Result;

                objOItem_Result = objDBLevel_Filter_Att.GetDataObjectAtt(objORel_Fields_att, doIds: false);

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID) return objOItem_Result;

                objOItem_Result = objDBLevel_RegEx_Att.GetDataObjectAtt(objORel_RegEx_Att, doIds: false);

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID) return objOItem_Result;

                objOItem_Result = objDBLevel_UserContentOfField.GetDataObjectRel(objORel_ContentField, doIds: false);

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID) return objOItem_Result;

                objOItem_Result = objDBLevel_ReplaceWith.GetDataObjectRel(objORel_Replacewith, doIds: false);

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID) return objOItem_Result;

                objOItem_Result = GetSubData_RegExReplace();

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID) return objOItem_Result;

                var searchContainFields = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = objLocalConfig.OItem_class_field.GUID,
                        ID_RelationType =
                            objLocalConfig.OItem_relationtype_contains.GUID,
                        ID_Parent_Other = objLocalConfig.OItem_class_field.GUID
                    }
                };

                objOItem_Result =
                    objDBLevel_ContainedFields.GetDataObjectRel(searchContainFields,
                        doIds: false);

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID) return objOItem_Result;

                var searchDoAll = objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectAtt
                {
                    ID_Object = f.ID_Other,
                    ID_AttributeType = objLocalConfig.OItem_attributetype_doall.GUID
                }).ToList();

                if (searchDoAll.Any())
                {
                    objOItem_Result = objDBLevel_DoAll.GetDataObjectAtt(searchDoAll,
                        doIds: false);
                }
                else
                {
                    objDBLevel_DoAll.ObjAtts.Clear();
                }
                
                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID) return objOItem_Result;

                var searchMergeFields = objDBLevel_FieldParser_To_Field.ObjectRels.Select(f => new clsObjectRel
                {
                    ID_Object = f.ID_Other,
                    ID_RelationType = objLocalConfig.OItem_relationtype_is.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_class_mergefield.GUID
                }).ToList();

                if (searchMergeFields.Any())
                {
                    objOItem_Result = objDBLevel_MergedFields.GetDataObjectRel(searchMergeFields);
                }
                else
                {
                    objDBLevel_MergedFields.ObjectRels.Clear();
                }
                

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID) return objOItem_Result;

                var searchMergeFieldsRel = objDBLevel_MergedFields.ObjectRels.Select(mergedField => new clsObjectRel
                {
                    ID_Object = mergedField.ID_Other,
                    ID_RelationType = objLocalConfig.OItem_relationtype_contains.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_class_field.GUID
                }).ToList();

                searchMergeFieldsRel.AddRange(objDBLevel_MergedFields.ObjectRels.Select(mergedField => new clsObjectRel
                {
                    ID_Object = mergedField.ID_Other,
                    ID_RelationType = objLocalConfig.OItem_relationtype_uses.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_class_text_seperators.GUID
                }));

                if (searchMergeFieldsRel.Any())
                {
                    objOItem_Result = objDBLevel_MergedFieldsRef.GetDataObjectRel(searchMergeFieldsRel);
                }
                else
                {
                    objDBLevel_MergedFieldsRef.ObjectRels.Clear();
                }

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID) return objOItem_Result;

                
                List<clsObjectRel> csvFields = objDBLevel_Fields_Rel.ObjectRels.Where(rel => rel.ID_Parent_Other == objLocalConfig.OItem_class_csv_field.GUID).ToList();
                List<clsRegExField> regExMain = (from objRegExMain in objDBLevel_Fields_Rel.ObjectRels.
                                                                Where(
                                                                    r =>
                                                                    r.ID_RelationType ==
                                                                    objLocalConfig
                                                                        .OItem_relationtype_main
                                                                        .GUID &&
                                                                    r.ID_Parent_Other ==
                                                                    objLocalConfig
                                                                        .OItem_class_regular_expressions
                                                                        .GUID).ToList()
                                                    join objRegExValMain in objDBLevel_RegEx_Att.ObjAtts on
                                                        objRegExMain.ID_Other equals objRegExValMain.ID_Object
                                                    select new clsRegExField
                                                    {
                                                        ID_Field = objRegExMain.ID_Object,
                                                        ID_RegEx = objRegExMain.ID_Other,
                                                        ID_Attribute = objRegExValMain.ID_Attribute,
                                                        RegEx = objRegExValMain.Val_String
                                                    }).ToList();

                List<clsRegExField> regExPre = (from objRegExPre in objDBLevel_Fields_Rel.ObjectRels.
                                                                            Where(
                                                                                r =>
                                                                                r.ID_RelationType ==
                                                                                objLocalConfig
                                                                                    .OItem_relationtype_pre
                                                                                    .GUID &&
                                                                                r.ID_Parent_Other ==
                                                                                objLocalConfig
                                                                                    .OItem_class_regular_expressions
                                                                                    .GUID).ToList()
                                                join objRegExValPre in objDBLevel_RegEx_Att.ObjAtts on
                                                    objRegExPre.ID_Other equals objRegExValPre.ID_Object
                                                select new clsRegExField
                                                {
                                                    ID_Field = objRegExPre.ID_Object,
                                                    ID_RegEx = objRegExPre.ID_Other,
                                                    ID_Attribute = objRegExValPre.ID_Attribute,
                                                    RegEx = objRegExValPre.Val_String
                                                }).ToList();

                List<clsRegExField> regExPost = (from objRegExPost in objDBLevel_Fields_Rel.ObjectRels.
                                                                            Where(
                                                                                r =>
                                                                                r.ID_RelationType ==
                                                                                objLocalConfig
                                                                                    .OItem_relationtype_posts
                                                                                    .GUID &&
                                                                                r.ID_Parent_Other ==
                                                                                objLocalConfig
                                                                                    .OItem_class_regular_expressions
                                                                                    .GUID).ToList()
                                                    join objRegExValPost in objDBLevel_RegEx_Att.ObjAtts on
                                                        objRegExPost.ID_Other equals objRegExValPost.ID_Object
                                                    select new clsRegExField
                                                    {
                                                        ID_Field = objRegExPost.ID_Object,
                                                        ID_RegEx = objRegExPost.ID_Other,
                                                        ID_Attribute = objRegExValPost.ID_Attribute,
                                                        RegEx = objRegExValPost.Val_String
                                                    }).ToList();

                FieldList = (from objField in objDBLevel_Fields.Objects1
                                join objFieldParser in objDBLevel_FieldParser_To_Field.ObjectRels on
                                    objField.GUID equals objFieldParser.ID_Other
                                join objRemoveFromSource
                                    in objDBLevel_Fields_Att.ObjAtts.
                                                            Where(
                                                                at =>
                                                                at.ID_AttributeType ==
                                                                objLocalConfig
                                                                    .OItem_attributetype_remove_from_source
                                                                    .GUID).ToList()
                                    on objField.GUID equals objRemoveFromSource.ID_Object
                                join objUseOrderId
                                    in objDBLevel_Fields_Att.ObjAtts.
                                                            Where(
                                                                at =>
                                                                at.ID_AttributeType ==
                                                                objLocalConfig
                                                                    .OItem_attributetype_useorderid.GUID)
                                                            .ToList()
                                    on objField.GUID equals objUseOrderId.ID_Object
                                join objHtmlDecode
                                    in objDBLevel_Fields_Att.ObjAtts.
                                                            Where(
                                                                at =>
                                                                at.ID_AttributeType ==
                                                                objLocalConfig
                                                                    .OItem_attributetype_htmldecode.GUID)
                                                            .ToList()
                                    on objField.GUID equals objHtmlDecode.ID_Object into objHtmlDecodes
                                from objHtmlDecode in objHtmlDecodes.DefaultIfEmpty()
                                join objDataType
                                    in objDBLevel_Fields_Rel.ObjectRels.
                                                            Where(
                                                                dt =>
                                                                dt.ID_RelationType ==
                                                                objLocalConfig
                                                                    .OItem_relationtype_value_type.GUID &&
                                                                dt.ID_Parent_Other ==
                                                                objLocalConfig.OItem_class_datatypes.GUID)
                                                            .ToList()
                                    on objField.GUID equals objDataType.ID_Object
                                join objMeta in objDBLevel_Fields_Rel.ObjectRels.
                                                                    Where(
                                                                        m =>
                                                                        m.ID_RelationType ==
                                                                        objLocalConfig
                                                                            .OItem_relationtype_is.GUID &&
                                                                        m.ID_Parent_Other ==
                                                                        objLocalConfig
                                                                            .OItem_class_metadata__parser_
                                                                            .GUID).ToList()
                                    on objField.GUID equals objMeta.ID_Object into objMetas
                                from objMeta in objMetas.DefaultIfEmpty()
                                join objUseLastValid in objDBLevel_Fields_Att.ObjAtts.
                                                                    Where(
                                                                        at =>
                                                                        at.ID_AttributeType ==
                                                                        objLocalConfig
                                                                            .OItem_attributetype_uselastvalid.GUID)
                                                                    .ToList() on objField.GUID equals objUseLastValid.ID_Object into UseLastValidItems
                                from objUseLastValid in UseLastValidItems.DefaultIfEmpty()
                                join objRegExMain in regExMain on objField.GUID equals objRegExMain.ID_Field into objRegExMains
                                from objRegExMain in objRegExMains.DefaultIfEmpty()
                                join objRegExPre in regExPre on objField.GUID equals objRegExPre.ID_Field into objRegExPres
                                from objRegExPre in objRegExPres.DefaultIfEmpty()
                                join objRegExPost in regExPost on objField.GUID equals objRegExPost.ID_Field into objRegExPosts
                                from objRegExPost in objRegExPosts.DefaultIfEmpty()
                                join objReplace in objDBLevel_ReplaceWith.ObjectRels on objField.GUID equals objReplace.ID_Object into objReplaces
                                from objReplace in objReplaces.DefaultIfEmpty()
                                join objReferenceField in objDBLevel_UserContentOfField.ObjectRels on objField.GUID equals objReferenceField.ID_Object into objReferenceFields
                                from objReferenceField in objReferenceFields.DefaultIfEmpty()
                                join objDoAll in objDBLevel_DoAll.ObjAtts on objField.GUID equals objDoAll.ID_Object into objDoAlls
                                from objDoAll in objDoAlls.DefaultIfEmpty()
                                join objFieldReplace in ReplaceList on objField.GUID equals objFieldReplace.ID_Field into objFieldReplaces
                                from objFieldReplace in objFieldReplaces.DefaultIfEmpty()
                                join objCSVField in csvFields on objField.GUID equals objCSVField.ID_Object into objCSVFields
                                from objCSVField in objCSVFields.DefaultIfEmpty()
                                select new clsField
                                {
                                    ID_FieldParser = objFieldParser.ID_Object,
                                    Name_FieldParser = objFieldParser.Name_Object,
                                    ID_Field = objField.GUID,
                                    Name_Field = objField.Name,
                                    ID_DataType = objDataType.ID_Other,
                                    DataType = objDataType.Name_Other,
                                    ID_Attribute_RemoveFromSource = objRemoveFromSource.ID_Attribute,
                                    RemoveFromSource = objRemoveFromSource.Val_Bit ?? false,
                                    ID_Attribute_UseOrderID = objUseOrderId.ID_Attribute,
                                    UseOrderId = objUseOrderId.Val_Bit ?? false,
                                    ID_MetaField = objMeta != null ? objMeta.ID_Other : null,
                                    Name_MetaField = objMeta != null ? objMeta.Name_Other : null,
                                    IsMeta = objMeta != null,
                                    ID_RegExPre = objRegExPre != null ? objRegExPre.ID_RegEx : null,
                                    ID_Attribute_RegExPreVal = objRegExPre != null ? objRegExPre.ID_Attribute : null,
                                    RegexPre = objRegExPre != null ? objRegExPre.RegEx : null,
                                    ID_RegExMain = objRegExMain != null ? objRegExMain.ID_RegEx : null,
                                    ID_Attribute_RegExMainVal = objRegExMain != null ? objRegExMain.ID_Attribute : null,
                                    Regex = objRegExMain != null ? objRegExMain.RegEx : null,
                                    ID_RegExPost = objRegExPost != null ? objRegExPost.ID_RegEx : null,
                                    ID_Attribute_RegExPostVal = objRegExPost != null ? objRegExPost.ID_Attribute : null,
                                    RegexPost = objRegExPost != null ? objRegExPost.RegEx : null,
                                    OrderId = objFieldParser.ID_RelationType == objLocalConfig.OItem_relationtype_starts_with.GUID ? -2 :
                                            objFieldParser.ID_RelationType == objLocalConfig.OItem_relationtype_ends_with.GUID ? -1 : objFieldParser.OrderID ?? 0,
                                    Insert = objReplace != null ? objReplace.Name_Other : null,
                                    IsInsert = objReplace != null ? true : false,
                                    ID_Attribute_UseLastValid = objUseLastValid != null ? objUseLastValid.ID_Attribute : null,
                                    UseLastValid = objUseLastValid != null ? objUseLastValid.Val_Bit != null ? (bool)objUseLastValid.Val_Bit : false : false,
                                    ID_ReferenceField = objReferenceField != null ? objReferenceField.ID_Other : null,
                                    Name_ReferenceField = objReferenceField != null ? objReferenceField.Name_Other : null,
                                    ID_Attribute_DoAll = objDoAll != null ? objDoAll.ID_Attribute : null,
                                    DoAll = objDoAll != null ? (bool)objDoAll.Val_Bool : false,
                                    ReplaceList = objFieldReplace != null ? objFieldReplace.ReplaceList : null,
                                    Start = objFieldParser.ID_RelationType == objLocalConfig.OItem_relationtype_starts_with.GUID ? true : false,
                                    End = objFieldParser.ID_RelationType == objLocalConfig.OItem_relationtype_ends_with.GUID ? true : false,
                                    HtmlDecode = objHtmlDecode != null ? objHtmlDecode.Val_Bit != null ? objHtmlDecode.Val_Bit.Value : false : false,
                                    ID_CSVField = objCSVField != null ? objCSVField.ID_Other : null,
                                    Name_CSVField = objCSVField != null ? objCSVField.Name_Other : null
                                }).ToList();

                
                FieldList.ForEach(field =>
                {
                    field.FieldListContained =
                        (from objField in
                            objDBLevel_ContainedFields.ObjectRels.OrderBy(orderField => orderField.OrderID).ThenBy(orderField => orderField.Name_Other)
                            where objField.ID_Object == field.ID_Field
                            join objSubField in FieldList on
                                objField.ID_Other equals
                                objSubField.ID_Field
                            select objSubField).ToList();

                    var mergedFields = objDBLevel_MergedFields.ObjectRels.Where(mergeF => mergeF.ID_Object == field.ID_Field).ToList();
                    if (mergedFields.Any())
                    {
                        field.MergeField = new MergeField();
                        field.MergeField.FieldList = (from mergeField in mergedFields
                                                        join fieldRel in objDBLevel_MergedFieldsRef.ObjectRels.Where(mergeFRel => mergeFRel.ID_Parent_Other == objLocalConfig.OItem_class_field.GUID) on mergeField.ID_Other equals fieldRel.ID_Object
                                                        join fieldItem in FieldList on fieldRel.ID_Other equals fieldItem.ID_Field
                                                        select fieldItem).ToList();
                        field.MergeField.SeperatorRelItem = (from mergeField in mergedFields
                                                                join seperatorItem in objDBLevel_MergedFieldsRef.ObjectRels on mergeField.ID_Other equals seperatorItem.ID_Object
                                                                select seperatorItem).FirstOrDefault();

                    }
                        
                });
                
            }

            return objOItem_Result;
        }

        private void Initialize()
        {
            objDBLevel_FieldToRegEx = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RegEx__Pattern = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_FieldParser_To_Field = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Fields = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Fields_Rel = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Fields_Att = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Filter_Att = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RegEx_Att = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_ReplaceWith = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_UserContentOfField = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_DoAll = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RegExReplace = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RegExOfReplace = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RegExRegEx = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RegExReplaceWith = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_ContainedFields = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_DocumentItem = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_DocItemToField = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_MergedFields = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_MergedFieldsRef = new OntologyModDBConnector(objLocalConfig.Globals);

            objTransaction = new clsTransaction(objLocalConfig.Globals);
            objRelationConfig = new clsRelationConfig(objLocalConfig.Globals);
        }
    }
}
