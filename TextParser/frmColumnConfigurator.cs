﻿using OModulesControls.ColumnFilter;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextParser
{
    public partial class frmColumnConfigurator : Form
    {
        UserControl_ColumnFilter userControl_ColumnFilter;

        private clsLocalConfig localConfig;

        private clsOntologyItem parentItem;

        private DataGridView dgvParent;

        public frmColumnConfigurator(clsLocalConfig localConfig, clsOntologyItem parentItem, DataGridView dgvParent)
        {
            this.localConfig = localConfig;
            this.parentItem = parentItem;
            this.dgvParent = dgvParent;

            InitializeComponent();

            var columns = dgvParent.Columns.Cast<DataGridViewColumn>().ToList();

            userControl_ColumnFilter = new UserControl_ColumnFilter();
            userControl_ColumnFilter.Globals = localConfig.Globals;
            userControl_ColumnFilter.GridItem = parentItem;
            userControl_ColumnFilter.GridColumns = columns;
            elementHost_ColumnConfigurator.Child = userControl_ColumnFilter;
        }

        private void toolStripButton_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
