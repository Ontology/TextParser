﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParser
{
    public class clsParseWindow
    {
        private clsLocalConfig objLocalConfig;

        private clsField startField;
        public clsField StartField
        {
            get { return startField; }
            set
            {
                startField = value;
                SetRegex(startField, true);
            }
        }
        private clsField endField;
        public clsField EndField
        {
            get { return endField; }
            set
            {
                endField = value;
                SetRegex(endField, false);
            }
        }

        public string RegexPre_Start { get; set; }
        public string RegexMain_Start { get; set; }
        public string RegexPost_Start { get; set; }

        public string RegexPre_End { get; set; }
        public string RegexMain_End { get; set; }
        public string RegexPost_End { get; set; }

        public bool StartChecked { get; set; }
        public bool EndChecked { get; set; }

        public bool CheckStart
        {
            get
            {
                return StartField != null;
            }
            
        }

        public bool CheckEnd
        {
            get
            {
                return EndField != null;
            }
        }

        private void SetRegex(clsField field, bool isStartField)
        {
            string regexPre = field.ID_RegExPre != null &&
                                       field.ID_RegExPre != objLocalConfig.OItem_object_empty.GUID
                                       ? field.RegexPre : null;
            string regexPost = field.ID_RegExPost != null &&
                               field.ID_RegExPost != objLocalConfig.OItem_object_empty.GUID
                ? field.RegexPost
                : null;

            string regexMain = field.ID_RegExMain != null &&
                               field.ID_RegExMain != objLocalConfig.OItem_object_empty.GUID
                               ? field.Regex : null;

            if (isStartField)
            {
                RegexPre_Start = regexPre;
                RegexMain_Start = regexMain;
                RegexPost_Start = regexPost;
            }
            else
            {
                RegexPre_End = regexPre;
                RegexMain_End = regexMain;
                RegexPost_End = regexPost;
            }
        }
        public bool DoParsing { get; set; }

        public clsParseWindow(clsLocalConfig localConfig)
        {
            objLocalConfig = localConfig;
        }
    }
}
