﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParser
{
    public class MergeField
    {
        public List<clsField> FieldList { get; set; }
        public string Id_Seperator
        {
            get
            {
                if (SeperatorRelItem != null)
                {
                    return SeperatorRelItem.ID_Other;
                }
                else
                {
                    return null;
                }
            }
            
        }
        public string Seperator
        {
            get
            {
                if (SeperatorRelItem != null)
                {
                    return SeperatorRelItem.Name_Other;
                }
                else
                {
                    return "";
                }
            }
        }
        public clsObjectRel SeperatorRelItem { get; set; }

        public string Value(Dictionary<string, object> fieldsWithValues)
        {
            
            var fieldValues = (from field in FieldList.OrderBy(field => field.OrderId)
                               join dictField in fieldsWithValues on field.Name_Field equals dictField.Key
                               select dictField.Value == null ? "" : dictField.Value.ToString()).ToList();
            if (fieldValues.Any(field => string.IsNullOrEmpty(field))) return null;
            return string.Join(Seperator, fieldValues);
        }

     
    }
}
