﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using Filesystem_Module;
using OntologyClasses.BaseClasses;
using TextParser.ViewModels;
using TextParser.Notifications;
using ElasticSearchNestConnector;
using System.IO;
using System.Text.RegularExpressions;

namespace TextParser
{
    public class clsCSVParser : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private clsDataWork_TextParser dataWorkTextParser;
        private List<clsField> fieldList;
        private clsUserAppDBSelector userAppDBSelector;
        private clsUserAppDBUpdater userAppDBUpdater;
        private List<clsFile> fileList;
        private int fileIx;
        public bool AbortParsing { get; set; }
        

        private clsOntologyItem resultParser;
        public clsOntologyItem ResultParser
        {
            get { return resultParser; }
            set
            {
                resultParser = value;
                RaisePropertyChanged(NotifyChanges.CSVParser_ResultParser);
            }
        }

        private string filePath;
        public string FilePath
        {
            get { return filePath; }
            set
            {
                filePath = value;
                RaisePropertyChanged(NotifyChanges.CSVParser_FilePath);
            }
        }

        public int FileCount
        {
            get { return fileList.Count; }
        }

        public int FileIx
        {
            get { return fileIx + 1; }
        }


        public clsCSVParser(clsLocalConfig localConfig, 
            clsDataWork_TextParser dataWorkTextParser, 
            List<clsField> fieldList,
            List<clsFile> fileList)
        {
            this.localConfig = localConfig;
            this.dataWorkTextParser = dataWorkTextParser;
            this.fieldList = fieldList;
            this.fileList = fileList;

            userAppDBSelector = new clsUserAppDBSelector(dataWorkTextParser.OItem_Server.Name,
                int.Parse(dataWorkTextParser.OItem_Port.Name),
                dataWorkTextParser.OItem_Index.Name.ToLower(),
                localConfig.Globals.SearchRange,
                localConfig.Globals.Session);

            userAppDBUpdater = new clsUserAppDBUpdater(userAppDBSelector);
        }

        public void Parse()
        {
            
            
            var result = localConfig.Globals.LState_Nothing.Clone();
            ResultParser = result;
            result = localConfig.Globals.LState_Success.Clone();

            var docList = new List<clsAppDocuments>();
            for (fileIx = 0; fileIx < fileList.Count; fileIx++)
            {
                
                if (AbortParsing)
                {
                    ResultParser = result;
                    break;
                }
                var file = fileList[fileIx];
                try
                {
                    var objFileMeta = new clsFileMeta();
                    objFileMeta.FilePath = file.FileName;
                    objFileMeta.FileName = Path.GetFileName(file.FileName);
                    objFileMeta.CreateDate = File.GetCreationTime(file.FileName).Date;
                    objFileMeta.LastWriteDate = File.GetLastWriteTime(file.FileName).Date;
                    objFileMeta.CreateDatetime = File.GetCreationTime(file.FileName);
                    objFileMeta.LastWriteTime = File.GetLastWriteTime(file.FileName);

                    DataTable dt = null;
                    
                    using (var fileStream = new FileStream(file.FileName, FileMode.Open))
                    {
                        using (var stream = new StreamReader(fileStream, Encoding.UTF8))
                        {
                            var fileContent = stream.ReadToEnd();
                            dt = DataTable.New.ReadFromString(fileContent);
                        }
                    }
                    
                        
                    var fileLine = 0;
                    foreach(Row row in dt.Rows)
                    {
                        var dict = new Dictionary<string, object>();
                        foreach(var field in fieldList.Where(fieldItem => fieldItem.Name_CSVField != null || fieldItem.IsInsert || fieldItem.IsMergeField || fieldItem.IsMeta))
                        {
                            if (field.IsMeta)
                            {
                                if (field.ID_MetaField == localConfig.OItem_object_filepath.GUID)
                                {
                                    dict.Add(field.Name_MetaField, objFileMeta != null ? objFileMeta.FilePath : "");
                                }
                                else if (field.ID_MetaField == localConfig.OItem_object_filename.GUID)
                                {
                                    dict.Add(field.Name_Field, objFileMeta != null ? objFileMeta.FileName : "");
                                }
                                else if (field.ID_MetaField == localConfig.OItem_object_guid.GUID)
                                {
                                    dict.Add(field.Name_Field, localConfig.Globals.NewGUID);
                                }
                                else if (field.ID_MetaField == localConfig.OItem_object_date.GUID)
                                {
                                    dict.Add(field.Name_Field, DateTime.Now.Date);
                                }
                                else if (field.ID_MetaField == localConfig.OItem_object_datetimestamp.GUID)
                                {
                                    dict.Add(field.Name_Field, DateTime.Now);
                                }
                                else if (field.ID_MetaField == localConfig.OItem_object_filedate__create_.GUID)
                                {
                                    dict.Add(field.Name_Field, objFileMeta != null ? objFileMeta.CreateDate : DateTime.Now.Date);
                                }
                                else if (field.ID_MetaField == localConfig.OItem_object_filedate__last_change_.GUID)
                                {
                                    dict.Add(field.Name_Field, objFileMeta != null ? objFileMeta.LastWriteDate : DateTime.Now.Date);
                                }
                                else if (field.ID_MetaField == localConfig.OItem_object_filedatetime__create_.GUID)
                                {
                                    dict.Add(field.Name_Field, objFileMeta != null ? objFileMeta.CreateDatetime : DateTime.Now);
                                }
                                else if (field.ID_MetaField ==
                                         localConfig.OItem_object_filedatetime__last_change_.GUID)
                                {
                                    dict.Add(field.Name_Field, objFileMeta != null ? objFileMeta.LastWriteTime : DateTime.Now);
                                }
                                else if (field.ID_MetaField == localConfig.OItem_object_fileline.GUID)
                                {
                                    dict.Add(field.Name_Field, fileLine);
                                }
                            }
                            else if (field.IsInsert)
                            {
                                dict.Add(field.Name_Field, field.Insert);
                            }
                            else if (field.IsMergeField)
                            {
                                var value = field.MergeField.Value(dict);
                                if (!string.IsNullOrEmpty(value))
                                {
                                    dict.Add(field.Name_Field, value);
                                }
                                
                            }
                            else if (row.ColumnNames.Contains(field.Name_CSVField))
                            {
                                var value = row[field.Name_CSVField];

                                if (field.ReplaceList != null && field.ReplaceList.Any())
                                {
                                    field.ReplaceList.Where(rep => !string.IsNullOrEmpty(rep.RegExSearch)).ToList().ForEach(rep =>
                                    {
                                        value = Regex.Replace(value, rep.RegExSearch,
                                            rep.ReplaceWith ?? "");
                                    });
                                }
                                if (field.ID_DataType == localConfig.OItem_object_bit.GUID)
                                {
                                    bool bValue;

                                    if (bool.TryParse(value,out bValue))
                                    {
                                        dict.Add(field.Name_Field, bValue);

                                    }
                                }
                                else if (field.ID_DataType == localConfig.OItem_object_datetime.GUID)
                                {
                                    DateTime dtValue;

                                    if (DateTime.TryParse(value, out dtValue))
                                    {
                                        dict.Add(field.Name_Field, dtValue);
                                    }
                                }
                                else if (field.ID_DataType == localConfig.OItem_object_int.GUID)
                                {
                                    long lValue;

                                    if (long.TryParse(value, out lValue))
                                    {
                                        dict.Add(field.Name_Field, lValue);
                                    }
                                }
                                else if (field.ID_DataType == localConfig.OItem_object_double.GUID)
                                {
                                    double dblValue;

                                    if (double.TryParse(value, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out dblValue))
                                    {
                                        dict.Add(field.Name_Field, dblValue);
                                    }
                                }
                                else
                                {
                                    dict.Add(field.Name_Field, value);
                                }
                                
                            }
                        }

                        if (dict.Any())
                        {
                            docList.Add(new clsAppDocuments
                            {
                                Id = localConfig.Globals.NewGUID,
                                Dict = dict
                            });
                        }

                        if (docList.Count > localConfig.Globals.SearchRange)
                        {
                            result =  userAppDBUpdater.SaveDoc(docList, dataWorkTextParser.OITem_Type.Name);
                            docList.Clear();
                            if (result.GUID == localConfig.Globals.LState_Error.GUID)
                            {
                                break;
                            }
                        }

                        fileLine++;
                    }


                    if (docList.Any())
                    {
                        result = userAppDBUpdater.SaveDoc(docList, dataWorkTextParser.OITem_Type.Name.ToLower());

                    }

                }
                catch(Exception ex)
                {
                    result = localConfig.Globals.LState_Error.Clone();
                }

            }

            ResultParser = result;
        }
    }
}
